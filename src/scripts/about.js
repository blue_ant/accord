(() => {
  var $col = $('.b-aboutpage__text:first');
  $('.b-aboutpage__text').niceScroll({
    cursorcolor: "#d3d3d3",
    cursoropacitymin: 0,
    cursoropacitymax: 0,
    cursorborder: "rgb(229,237,242)",
    autohidemode: false,
    enablekeyboard: false,
    smoothscroll: true,
  });

  let $cont = $(".l-about:first").parent();


  let $asideFormForCorporateSalePage = $cont.find('#createOrderForCorporateForm');

  let corporateOrderFormInst;
  if ($asideFormForCorporateSalePage.length) {
    corporateOrderFormInst = new InteractiveForm({
      el: $asideFormForCorporateSalePage.find("form:first"),
      successTextClass: "b-aside-form__success-text"
    });

  }

  $cont.on("transitEnd", () => {
    $col.getNiceScroll().remove();

    if ($asideFormForCorporateSalePage.length) {
      corporateOrderFormInst.destroy();
      corporateOrderFormInst = null;
    }

  });
})();