class LastNewsBlock {
    constructor(opts) {

        let $el = $(opts.el);
        let $arrows = $el.find(".b-hpnews__xsarrow");
        let $halfs = $el.find(".b-hpnews__half");

        $arrows.on('click', (event) => {
            event.preventDefault();
            window.requestAnimationFrame(() => {
            	$arrows.toggleClass('b-hpnews__xsarrow--active');
                $halfs.toggleClass('b-hpnews__half--active');

                let newBlockTitle = $halfs.filter(".b-hpnews__half--active").find('.b-hpnews__title:first').html();
                $el.find('.b-hpnews__xstitle:first').html(newBlockTitle);
                
            });

            return false;
        });

        this.$el = $el;
    }
}