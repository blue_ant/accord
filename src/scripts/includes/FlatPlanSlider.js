class FlatplanSlider {
    constructor(el) {
        let $el = $(el).eq(0);

        if ($el.length === 0) {
            console.error("FlatplanSlider constructor can't find target element");
            return;
        }

        let $plans = $el.find(".FlatplanSlider_plansRow");
        let plansSlider = new Swiper($plans, {
            slidesPerView: 1,
            effect: "fade",
            loop: true,
            fade: {
                crossFade: true,
            },
        });

        let $titles = $el.find(".FlatplanSlider_titlesRow");
        let $titlesTextElements = $titles.find(".FlatplanSlider_title");
        let titlesWidths = [];

        $titlesTextElements.each(function(index, el) {
            titlesWidths.push($(el).width());
        });

        let $underline = $el.find(".FlatplanSlider_titlesUnderline");

        let titlesSlider = new Swiper($titles, {
            slidesPerView: 1,
            loop: true,
            prevButton: $titles.find(".FlatplanSlider_titlesArrow-prev"),
            nextButton: $titles.find(".FlatplanSlider_titlesArrow-next"),
            onSlideChangeEnd: (swiper) => {
                $underline.css("width", `${titlesWidths[swiper.realIndex]}px`);
            },
            onInit: () => {
                $underline.css("width", `${titlesWidths[0]}px`);
            },
        });

        titlesSlider.params.control = plansSlider;
        plansSlider.params.control = titlesSlider;

        this.$el = $el;
        this.titlesSlider = titlesSlider;
        this.plansSlider = plansSlider;
    }

    destroy() {
        titlesSlider.destroy();
        plansSlider.destroy();
    }
}
