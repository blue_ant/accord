class MortgageMortgageBanklist {
    constructor(opts = {}) {
        
        let tplStr = `
 <div class="MortgageBanklist">
    <% _.forEach(banks, function(bank){ %>
        <div class="MortgageBanklist_row">
            <div class="MortgageBanklist_cell MortgageBanklist_cell-img"><div class="MortgageBanklist_logoPic" title="<%= bank.name %>" style="background-image: url('<%= bank.logo %>')"></div></div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-description">
                <h4 class="MortgageBanklist_header"><span class="hidden-sm">Первоначальный </span>взнос</h4>
                <p class="MortgageBanklist_text">
                    <% if(bank.firstFrom === bank.firstTo) {%>
                        <%= bank.firstFrom %>
                    <%}else if(bank.firstTo === 100){%>
                        от <%= bank.firstFrom %>
                    <%}else{%>
                        <%= bank.firstFrom %> - <%= bank.firstTo %>
                    <%};%>%
                </p>
            </div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-rate">
                    <% if(bank.isHinted) {%>
                        <h4 class="MortgageBanklist_header MortgageBanklist_header-star">Ставка</h4>
                    <%}else{%>
                        <h4 class="MortgageBanklist_header">Ставка</h4>
                    <%};%>
                <p class="MortgageBanklist_text" style="text-align:center;">
                    <% if(bank.rateFrom === bank.rateTo) {%>
                        <%= bank.rateFrom %>
                    <%}else{%>
                        <%= bank.rateFrom %> - <%= bank.rateTo %>
                    <%};%>%
            </p>
            </div>
            <div class="MortgageBanklist_cell">
                <h4 class="MortgageBanklist_header">Срок кредита до</h4>
                <p class="MortgageBanklist_text"><%= bank.yearsTo %> лет</p>
            </div>
            <div class="MortgageBanklist_cell">
                <h4 class="MortgageBanklist_header">Платежи в месяц</h4>
                <p class="MortgageBanklist_text"><%= bank.monthlyPayment %> ₽</p>
            </div>
        </div>
        <%}); %>
        <p class="MortgageBanklist_tipText"><span class="MortgageBanklist_tipSign">*</span>подробности предоставления данных условий уточняйте у менеджера по ипотеке.</p>
</div>
`;

        //trim whitespaces for faster rendering
        tplStr = tplStr.replace(/\s{2,}/g, "");

        this.compiledTemplate = _.template(tplStr);
        this.$mountEl = $(opts.mountEl);
    }

    prepareData(data) {
        let opts = data.opts;

        data.banks = _.sortBy(data.banks, "rateFrom");

        for (let i = data.banks.length - 1; i >= 0; i--) {
            let bnkdt = data.banks[i];

            // prepare banks some datas for rendering
            if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
            bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

            //calculate monthly payment
            {
                let c = opts.price - opts.firstPay; // сумма кредита
                let p = bnkdt.rateAverage; // годовая процентная ставка
                let n = opts.duration * 12; // срок кредита в месяцах
                let a = 1 + p / 1200; // знаменатель прогрессии
                let k = (Math.pow(a, n) * (a - 1)) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
                let sm = k * c; // ежемесячный платёж

                bnkdt.monthlyPayment = parseInt(sm).toLocaleString("ru-RU");
            }
        }

        return data;
    }

    renderList(data) {
        let dataToRender = this.prepareData(data);
        this.$mountEl.html(this.compiledTemplate(dataToRender));
    }
}
