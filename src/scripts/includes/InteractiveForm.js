$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Поле заполнено не верно"
);
class InteractiveForm {
    constructor(opts) {
        this.$form = $(opts.el).eq(0);
        let $form = this.$form;

        let successTextClass = opts.successTextClass || "interactive-form-success-text";
        let successTextTag = opts.successTextTag || "div";

        $form.find('[name="phone"]').inputmask({
            mask: "+7 999 999-9999",
            showMaskOnHover: false,
        });
        $form.find('[name="time"]').inputmask("hh:mm  -  hh:mm", {

            showMaskOnHover: false
        });
        $form.find('[name="time_from"],[name="time_to"]').inputmask("hh:mm", {
            showMaskOnHover: false
        });
        $form.find('[name="captcha"]').inputmask({
            mask: "9999",
            showMaskOnHover: false
        });

        let validatorOpts = {
            rules: {
                phone: {
                    required: true,
                    regex: /\+7\s\d\d\d\s\d\d\d\-\d\d\d\d/
                },
                captcha: {
                    required: true,
                    regex: /\d\d\d\d/
                },
                time: {
                    required: true,
                    regex: /\d\d\:\d\d\s\s-\s\s\d\d\:\d\d/
                },

            },
            errorPlacement: ($errorLabel, $el) => {

                if ($el.is(".b-custom-checkbox__input")) {
                    return true;
                } else {
                    $errorLabel.insertAfter($el);
                }

            },

            onfocusout: (el, event) => {
                $(el).valid();
            },
            onkeyup: false,
            onclick: false,
            focusCleanup: false,

            submitHandler: function(form) {
                let $form = $(form);
                let $btn = $form.find('[type="submit"]:first');
                $btn.attr('disabled', true);
                showPagePreloader();
                $.ajax({
                    url: $form.data('action') || $form.attr("action"),
                    type: $form.attr("method") || "POST",
                    data: `${$form.serialize()}&sessionId=${window.call_value_2}`,
                    dataType: "json"
                }).done(function(data) {
                    let errorCode = parseInt(data.code);

                    if (errorCode === 0) {

                        TweenLite.to($form, 0.2, {
                            scale: 0,
                            ease: Power1.easeIn,
                            onComplete: () => {
                                if (opts.beforeSuccessTextInsert) {
                                    opts.beforeSuccessTextInsert($form);
                                }
                                $btn.removeAttr('disabled');
                                $form
                                    .after(`<${successTextTag} class="${successTextClass}">${data.success}</${successTextTag}>`)
                                    .hide()
                                    .parent()
                                    .find(".b-aside-form__header:first")
                                    .hide();
                                if (opts.afterSuccessTextInsert) {
                                    opts.afterSuccessTextInsert($form);
                                }

                                fireTrackersOnSubmit($form.data('action'));
                            }
                        });

                    } else if (errorCode === 1) {
                        let validator = $form.data('validator');
                        $btn.removeAttr('disabled');
                        $form.find('[name="captcha"]:first').trigger('focus');
                        $form.find('img:first').trigger('click');
                        //$form.find('label[for="captcha"]');
                        validator.showErrors({
                            captcha: data.error,
                        });
                    }
                }).always(() => {
                    hidePagePreloader();
                });
                return false;
            }
        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        this.validator = $form.validate(validatorOpts);

    }

    destroy() {
        this.validator.destroy();
        this.$form.find('input').inputmask('remove');
    }

}