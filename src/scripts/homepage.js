(() => {

    let homepageForm = new InteractiveForm({
        el: ".b-prospage-callback-form__form:first",
        successTextClass: "b-prospage-callback-form__success-text",
        beforeSuccessTextInsert: ($form) => {
            $(".b-prospage-callback-form__lead:first").remove();
        },
        validatorParams: {
            highlight: function(element, errorClass, validClass) {
                if (element.name === "agree") {
                    element.classList.add("error");
                } else {
                    $(element)
                        .addClass('b-prospage-callback-form__input--error')
                        .parent()
                        .find(".b-prospage-callback-form__label")
                        .removeClass(validClass)
                        .addClass('b-prospage-callback-form__label--error');
                }

            },
            unhighlight: function(element, errorClass, validClass) {
                if (element.name === "agree") {
                    element.classList.remove("error");
                } else {

                    $(element)
                        .removeClass('b-prospage-callback-form__input--error')
                        .parent()
                        .find(".b-prospage-callback-form__label")
                        .removeClass("b-prospage-callback-form__label--error")
                        .addClass(validClass)
                        .html("введите ваш номер");
                }
            },
            errorPlacement: function(error, element) {
                $(element)
                    .parent()
                    .find(".b-prospage-callback-form__label")
                    .html(error.html());
            },
        }
    });

    let $cont = $(".b-homepage-slides:first").parent();
    window.requestAnimationFrame(() => {
        $cont.find('[data-delayed-src]').each(function(index, el) {
            let $pic = $(el);
            $pic.attr('src', $pic.data('delayed-src'));
        });
    });

    new LastNewsBlock({
        el: "#homepageLastNewsBlock"
    });

    let tl = new TimelineMax();

    let homepageInit = () => {


        let homepageSlider = new Swiper('.b-homepage-slides', {
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            effect: "fade",
            speed: 1000,
        });

        let $progressbar = $(".b-bottom-progressbar__track:first");
        let video = $(".b-homepage-videobg:first").get(0);
        let $headers = $(".b-homepage-heading:first");
        let $actionList = $("#homepageLastNewsBlock");
        let $slide2img = $(".b-prospage-pics--slide-1:first .b-prospage-pics__img:first");
        let $slide2textblock = $(".b-pros--1:first");
        let $slide2pins = $(".b-pinbtn--pros-1-1:first, .b-pinbtn--pros-1-2:first");
        let $slide3imgs = $(".b-prospage-pics__wrap--2:first .b-prospage-pics__img");
        let $slide3textblock = $(".b-pros--2:first");
        let $slide3pin = $(".b-pinbtn--pros-2-1:first");
        let $slide4img = $(".b-prospage-pics__wrap--3:first .b-prospage-pics__img");
        let $slide4textblock = $(".b-pros--3:first");
        let $slide4linePins = $(".b-pinbtn--pros-3-1:first,.b-pinbtn--pros-3-2:first");
        let $slide4innerPin = $(".b-pinbtn--pros-3-3:first");
        let $slide5imgs = $(".b-prospage-pics__wrap--4:first .b-prospage-pics__img");
        let $slide5textblock = $(".b-pros--4:first");
        let $slide5pin = $(".b-pinbtn--pros-4-1:first");
        let $slide6imgs = $(".b-prospage-pics__wrap--5:first .b-prospage-pics__img");
        let $slide6textblock = $(".b-pros--5:first");
        let $slide6pins = $(".b-pinbtn--pros-5-1:first,.b-pinbtn--pros-5-2:first");
        let $slide7form = $(".b-prospage-callback-form");

        const defaultSlideDelay = 4;
        let isAlreadyWasStarted = false;
        

        tl
            .addLabel("animeStart")
            .set(document.querySelector(".l-topbar"),{className: "+=l-topbar--white"})
            .call(() => {
                homepageSlider.slideTo(0);
                video.pause();
                video.currentTime = 0;
                if (!isAlreadyWasStarted) {
                    video.play();
                }
                
            })
            .addLabel("showSlide1", `animeStart+=18.5`)
            //SLIDE 0 TO 1
            .set($(".l-footer:first"), { className: "+=visible-xs" }, `showSlide1`)

            .to([$headers, $actionList], 1, {
                opacity: 0,
                y: 90
            })
            .set(document.querySelector(".l-topbar"),{className: "-=l-topbar--white"})
            .call(() => {
                homepageSlider.slideTo(1);
            })
            .from($slide2img, 1, {
                y: -1 * $(window).height(),
                opacity: 0,
                ease: Power1.easeOut,
                delay: 1,
                onComplete: ()=>{
                    video.pause();
                }
            })
            .from($slide2textblock, 1, {
                y: -100,
                opacity: 0,
            })

            .staggerFrom($slide2pins, 1, {
                opacity: 0,
                y: -90
            }, 0.2)
            //SLIDE 1 TO 2
            .staggerTo($slide2pins, 1, {
                y: 80,
                opacity: 0,
                delay: defaultSlideDelay,
            })
            .addLabel("hideSlide2imgAndTextblock")
            .to($slide2textblock, 1, {
                y: 100,
                opacity: 0
            }, "hideSlide2imgAndTextblock")
            .to($slide2img, 1, {
                y: parseInt($(window).height() / 4),
                opacity: 0
            }, "hideSlide2imgAndTextblock")
            .call(() => {
                homepageSlider.slideTo(2, 0, false);
            })
            .addLabel("showSlide3items")
            .staggerFrom($slide3imgs, 1, {
                y: -1 * $(window).height(),
                opacity: 0
            }, 0.1, "showSlide3items")

            .from($slide3textblock, 1, {
                y: -90,
                opacity: 0,
                delay: 0.5,
                ease: Power1.easeIn,
            }, "showSlide3items+=0.5")
            .from($slide3pin, 0.6, {
                opacity: 0,
                ease: Power1.easeIn,
            })
            //SLIDE 2 TO 3
            .to($slide3pin, 0.8, {
                opacity: 0,
                y: 34,
                delay: defaultSlideDelay
            })
            .addLabel("hideSlide3TextAndImg")
            .to($slide3textblock, 1, {
                opacity: 0,
                y: 150
            }, "hideSlide3TextAndImg")
            .staggerTo($slide3imgs, 1, {
                opacity: 0,
                y: $(window).height(),
                ease: Power2.easeIn
            }, 0.1, "hideSlide3TextAndImg")
            .call(() => {
                homepageSlider.slideTo(3, 0, false);
            })
            .addLabel("showSlide4items")
            .from($slide4textblock, 1, {
                x: -80,
                opacity: 0
            }, "showSlide4items")
            .from($slide4img, 1, {
                x: "100%",
                opacity: 0,
            }, "showSlide4items")
            .staggerFrom($slide4linePins, 1, {
                x: -60,
                opacity: 0
            }, 0.2)
            .from($slide4innerPin, 0.5, {
                opacity: 0,
                scaleX: 0.6,
                scaleY: 0.6,
            })
            //SLIDE 3 TO 4
            .addLabel("hideSlide4pins")
            .to($slide4innerPin, 1, {
                scaleX: 0.6,
                scaleY: 0.6,
                opacity: 0,
            }, `hideSlide4pins+=${defaultSlideDelay}`)
            .staggerTo($slide4linePins, 1, {
                delay: 0.1,
                x: -80,
                opacity: 0
            }, 0.3, `hideSlide4pins+=${defaultSlideDelay}`)
            .addLabel("hideSlide4imgAndText")
            .to($slide4img, 1, {
                opacity: 0,
                x: "50%",
                ease: Power2.easeIn
            }, "hideSlide4imgAndText")
            .to($slide4textblock, 1, {
                x: -100,
                opacity: 0,
                ease: Power2.easeIn,
            }, "hideSlide4imgAndText")
            .call(() => {
                homepageSlider.slideTo(4, 0, false);
            })
            .staggerFrom($slide5imgs, 1, {
                y: -$(window).height(),
                opacity: 0,
                ease: Power2.easeInOut
            }, 0.1)
            .addLabel("showSlide5TextAndPins")
            .set($slide5pin, {
                clearProps: "opacity,visibility"
            }, "showSlide5TextAndPins")
            .from($slide5pin, 0.4, {
                opacity: 0,
                y: 50
            }, "showSlide5TextAndPins")
            .from($slide5textblock, 1, {
                x: -100,
                opacity: 0
            }, "showSlide5TextAndPins")
            .to($slide5pin, 0.5, {
                opacity: 0,
                y: 50,
                delay: defaultSlideDelay
            })
            .addLabel("hideSlide5imgAndText")
            .to($slide5textblock, 1, {
                y: 100,
                opacity: 0
            }, "hideSlide5imgAndText")
            .staggerTo($slide5imgs, 1, {
                y: $(window).height(),
                opacity: 0,
                ease: Power2.easeIn
            }, 0.1, "hideSlide5imgAndText")
            .call(() => {
                homepageSlider.slideTo(5, 0, false);
            })
            .addLabel("showSlide6textAndImg")
            .from($slide6textblock, 1, {
                opacity: 0,
                x: -100,
                ease: Power2.easeOut,

            }, `showSlide6textAndImg`)
            .staggerFrom($slide6imgs, 1, {
                y: -$(window).height(),
                opacity: 0,
                ease: Power2.easeOut
            }, 0.1, `showSlide6textAndImg`)
            .staggerFrom($slide6pins, 1, {
                y: -50,
                opacity: 0
            }, 0.1)
            .addLabel(`hideSlide6pins`)
            .staggerTo($slide6pins, 1, {
                x: -50,
                opacity: 0
            }, 0.1, `hideSlide6pins+=${defaultSlideDelay}`)
            .addLabel(`hideSlide6textAndImg`)
            .to($slide6textblock, 1, {
                y: 100,
                opacity: 0
            }, 'hideSlide6textAndImg')
            .staggerTo($slide6imgs, 1, {
                y: $(window).height(),
                opacity: 0,
                ease: Power2.easeInOut
            }, 0.2, 'hideSlide6textAndImg')
            .call(() => {
                homepageSlider.slideTo(6, 0, false);
            })
            .set($(".l-footer:first"), { className: "-=visible-xs" })
            .from($slide7form, 1, {
                opacity: 0,
                ease: Power2.easeInOut,
                scaleY: 0.5,
                scaleX: 0.5,
            })
            .to($progressbar, 69, {
                width: '100%',
                ease: Power0.easeNone,
            }, "animeStart");

        $(".b-homepage-heading__btn:first").off().on('click.homepage', () => {
            showPagePreloader();
            $cont.waitForImages(() => {
                hidePagePreloader();
                tl.seek("showSlide1");
                tl.play();
                isAlreadyWasStarted = true;
            });
            return false;
        });

        $('.b-logo:first').on('click.homepage', function(event) {
            event.preventDefault();
            isAlreadyWasStarted = true;
            tl.seek(0);
            tl.pause();
            homepageSlider.slideTo(0);
            return false;
        });
    };

    if (document.readyState === "complete") { homepageInit(); } else {
        $(window).one('load', homepageInit);
    }

    $cont.on('transitStart', (event) => {
        tl.kill();
        $('.b-logo:first').off('.homepage');
    });

    $cont.on('transitEnd', (event) => {
        $(".l-footer:first").removeClass('visible-xs');
    });

})();