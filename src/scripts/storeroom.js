(() => {
    insertGenplanModal();

    var $genplanModal = $("#genplanModal");

    var $cont = $(".l-storerooms:first").parent();
    var getFlatForm = new InteractiveForm({ el: "#form6", successTextClass: "b-aside-form__success-text" });
    var scroller1 = $cont.find('.b-flats:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#storeroomsPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    var rangeSlider1 = $cont.find('[data-area-slider]:first').bootstrapSlider({
        tooltip: 'hide'
    }).on("slide", function(event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#storeroomsAreaMin").html(min);
        $("#storeroomsAreaMax").html(max);
        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    var storeroomCellTpl = Handlebars.compile(`

{{#if flats}}
{{#each flats}}
<div class="b-flats__cell col-xs-12 col-sm-6 col-md-4">
    <div class="b-flats__cell-inner">
        <div class="b-flatcard hidden-xs">
            <div class="b-flatcard__row">
                <div class="b-flatcard__cell">
                    <div class="va">
                        <div class="va-item b-flatcard__title">№{{name}}</div>
                        <div class="va-item b-flatcard__house-title">корпус&nbsp;{{house}}</div>
                    </div>
                </div>
            </div>
            <div class="b-flatcard__row b-flatcard__row--middle">
                <div class="b-flatcard__cell">
                    <div class="b-flatcard__img b-flatcard__img--with-btn" style="background-image: url({{img}});"> <a href="#" data-aside-menu="getPriceAsideForm" class="btn btn-xl btn-primary b-flatcard__btn">Узнать стоимость</a> </div>
                </div>
            </div>
            <div class="b-flatcard__row">
                <div class="b-flatcard__cell">
                    <div class="va">
                        <div class="va-item b-flatcard__footage"> {{area}} m<sup>2</sup></div>
                        <div class="va-item b-flatcard__entrance-num">подъезд&nbsp;{{section}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-mobile-flatcard container-fluid visible-xs">
            <div class="row">
                <div class="col-xs-7"> <img src="{{img}}" alt="" class="b-mobile-flatcard__img img-responsive center-block"> </div>
                <div class="col-xs-5">
                    <div class="b-mobile-flatcard__title">№{{name}}</div>
                    <div class="b-mobile-flatcard__footage"> {{area}} m<sup>2</sup></div>
                    <div class="b-mobile-flatcard__footage">корпус&nbsp;{{house}}</div>
                    <div class="b-mobile-flatcard__footage">подъезд&nbsp;{{section}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
{{/each}}

{{else}}
<div class="b-flats__empty">
    <div class="b-flats__empty-inner">
        <img src="/img/cloud.svg" alt="" class="img-responsive">
        <div class="b-flats__empty-header">НЕТ РЕЗУЛЬТАТОВ</div>
        <div class="b-flats__empty-text">Планировки по заданным параметрам не найдены,<br>
пожалуйста измените параметры поиска.</div>
        <img src="/img/crane.svg" alt="" class="b-flats__empty-bootom-img">
    </div>
</div>
{{/if}}

`);

    $(document).on('click.storeroomspage', '.b-flats-filter__apply-btn', function(event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    });

    var $filterForm = $cont.find("#formFilterStorerooms");
    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage",
        skip: ["area_to", "area_from"]
    });

    $filterForm.find("input").on("change", _.debounce(function() {
        var $nonEmptyInputs = $filterForm.find("input").filter(function(index, element) {
            return $(element).val() !== "";
        });
        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage",
            skip: ["area_to", "area_from"]
        });
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function(event) {
        event.preventDefault();
        showPagePreloader();
        var $nonEmptyInputs = $filterForm.find("input").filter(function(index, element) {
            return $(element).val() !== "";
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }

        $.ajax({
            url: '/storerooms/?ajax=1&get_flats=1&filter=1' + urlParams

        }).done(function(data) {
            var markup = storeroomCellTpl(data);
            $cont.find("#storeroomCellsContainer").html(markup);
            window.accordAppStates.isPreloaderLockedOn = false;
            hidePagePreloader();
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.on('transitStart', function(event) {
        genplanHide();
        window.accordAppStates.isPreloaderLockedOn = false;
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document]).off(".storeroomspage");
    }).on('transitEnd', function(event) {

        rangeSlider1.bootstrapSlider('destroy');

        scroller1.remove();
        scroller2.remove();
        getFlatForm.destroy();
        getFlatForm = null;
    });
})();
