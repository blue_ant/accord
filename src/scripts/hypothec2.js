(() => {
    let $cont = $(".l-hypothec:first").parent();
    let $col = $cont.find(".b-hypothec__right:first");
    $col.niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 },
    });

    let hypothecOrderForm = new InteractiveForm({
        el: $cont.find(".b-aside-form--hypothec form:first"),
        successTextClass: "b-aside-form__success-text",
    });

    $cont.find(".selectpicker").selectpicker({
        //style: 'form-control b-aside-form__inp',
    });
    $cont.find(".b-mobile-hypothec__btn").click(function(e) {
        e.preventDefault();
        $(".b-mobile-hypothec__btn").removeClass("b-mobile-hypothec__btn--active");
        $(this)
            .tab("show")
            .addClass("b-mobile-hypothec__btn--active");
    });

    $cont.find(".b-hypothec__bubble-ico").popover({
        html: true,
        trigger: "hover",
    });

    $cont
        .on("transitStart", (event) => {
            $cont.find(".b-hypothec__bubble-ico").popover("destroy");
        })
        .on("transitEnd", (event) => {
            $cont.find(".selectpicker").selectpicker("destroy");
            $col.getNiceScroll().remove();
            hypothecOrderForm.destroy();
        });
})();
