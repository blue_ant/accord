(() => {

    let $cont = $(".l-infras--external:first").parent();

    let accordBuildingsCoords = {
        lat: 55.607676,
        lng:  37.104740
    };
    let map = new google.maps.Map(document.getElementById('infrasMap'), {
        zoom: 12,
        center: { lat: accordBuildingsCoords.lat + 0.01, lng: accordBuildingsCoords.lng },
        streetViewControl: false,
    });
    let markers = _.sortBy(window.accordAppStates.ifrasObjs, function(o) {
        return o.coords.lat;
    });

    let insertedMarkers = [];

    for (let i = 0; i < markers.length; i++) {

        let image = {
            url: markers[i].ico,
            scaledSize: new google.maps.Size(104, 58),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(78, 58)
        };
        let infowindow = new google.maps.InfoWindow({
            content: markers[i].title,
            pixelOffset: new google.maps.Size(26, 0)
        });
        let marker = new google.maps.Marker({
            position: markers[i].coords,
            map: map,
            icon: image,
            iconsType: markers[i].type,
        });
        marker.addListener('click', () => {
            infowindow.open(map, marker);
        });

        insertedMarkers.push(marker);
    }

    var mainPin = {
        url: "/img/contacts/logo_contact.png",
        scaledSize: new google.maps.Size(134, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(100, 100)

    };
    var mainMarker = new google.maps.Marker({
        position: accordBuildingsCoords,
        map: map,
        icon: mainPin,
        iconsType: "unbeatable",
    });

    insertedMarkers.push(mainMarker);

    $cont.find(".b-taglist__link").on('click', (event) => {
        event.preventDefault();
        let $tag = $(event.currentTarget);
        let $pins = $(".b-contacts-map__pin");

        $tag.toggleClass('b-taglist__link--active');

        let $activeTags = $(".b-taglist__link--active");
        if (!$activeTags.length) {
            for (let i = insertedMarkers.length - 1; i >= 0; i--) {
                insertedMarkers[i].setVisible(true);
            }
        } else {
            let activeTypes = [];
            $activeTags.each((index, el) => {
                activeTypes.push(parseInt(el.dataset.iconsType));
            });

            for (let i = insertedMarkers.length - 1; i >= 0; i--) {
                if(insertedMarkers[i].iconsType === "unbeatable"){
                    continue;
                }else if (_.includes(activeTypes, insertedMarkers[i].iconsType)) {
                    insertedMarkers[i].setVisible(true);
                } else {
                    insertedMarkers[i].setVisible(false);
                }
            }
        }

    });

    $cont
    /* .on('transitStart', (event) => {

     })*/
        .on('transitEnd', (event) => {
        map = insertedMarkers = null;
    });

})();
