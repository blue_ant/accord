(() => {

    let $cont = $(".l-baby:first").parent();
    let bgEl1 = $cont.find(".b-parralax-bg__track--baby-1:first").get(0);
    let bgEl2 = $cont.find(".b-parralax-bg__track--baby-2:first").get(0);
    let $col = $cont.find('.b-centered-text:first');

    $col.on('scroll', (event) => {
        let $col = $(event.target);
        let onePercent = $col.height() / 100;
        let currentScroll = $col.scrollTop();
        let slip = parseInt(currentScroll / onePercent);

        TweenLite.set(bgEl1, { y: -slip * 2 });
        TweenLite.set(bgEl2, { y: -slip });
    }).niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "#d3d3d3",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset:{top:-8},

    });

    $cont.on("transitEnd", () => {
        $col.getNiceScroll().remove();
    });
})();
