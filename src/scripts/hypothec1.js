(() => {
    let $cont = $(".l-hypothec:first").parent();
    let $col = $(".b-hypothec__right .MortgageBanklistWrapper");

    $col.niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: 10, left: 8 },
    });

    let forms = [];

    $(".Subscribe_form").each(function(index, el) {
        console.log(index);
        forms.push(
            new InteractiveForm({
                el: el,
                successTextClass: "Subscribe_successText",
            })
        );
    });

    $cont.find(".selectpicker").selectpicker({
        //style: 'form-control b-aside-form__inp',
    });
    $cont.find(".b-mobile-hypothec__btn").click(function(e) {
        e.preventDefault();
        $(".b-mobile-hypothec__btn").removeClass("b-mobile-hypothec__btn--active");
        $(this)
            .tab("show")
            .addClass("b-mobile-hypothec__btn--active");
    });

    $cont.find(".b-hypothec__bubble-ico").popover({
        html: true,
        trigger: "hover",
    });

    let MortgageBanklistRenderer = new MortgageMortgageBanklist({
        mountEl: "#MortgageBanklistContainer",
    });

    let checkBank = (singleBankParams, opts) => {
        let price = opts.price;
        let firstPay = opts.firstPay;
        let duration = opts.duration;

        // check by first pay party
        let firsPayPartyPercent = Math.round(firstPay / (price / 100));

        if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

        if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo)
            return false;

        // check by pay duration range
        if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;

        return true;
    };

    let pickupAndRenderBanks = _.debounce(() => {
        let allBanksParams = window.banksMortgageConditions;
        let selectedOpts = $calculatorForm.serializeObject();

        // convert all selected values to 'INT' type
        selectedOpts = _.mapValues(selectedOpts, (param) => {
            return parseInt(param);
        });

        let acceptableBanks = [];

        // filter banks
        allBanksParams.forEach((bankParams) => {
            if (checkBank(bankParams, selectedOpts)) {
                acceptableBanks.push(bankParams);
            }
        });

        // render banks
        let listMarkup = MortgageBanklistRenderer.renderList({
            banks: acceptableBanks,
            opts: selectedOpts,
        });
    }, 100);

    let $calculatorForm = $(".MortgageCalc");
    $calculatorForm.on("submit", function(event) {
        event.preventDefault();
        return false;
    });

    {
        let $formSliderBlock = $calculatorForm.find(".MortgageCalc_option-firstPayment");

        var firstPaymentSliderInst = new MortgageSlider(
            $formSliderBlock.find(".MortgageSlider"),
            {
                change: () => {
                    pickupAndRenderBanks();
                },
            },
            {
                slideEventCallback: (/*event, ui*/) => {
                    let selectedFirstPayment = firstPaymentSliderInst.getValue();
                    let selectedPrice = priceSliderInst.getValue();

                    if (selectedFirstPayment >= selectedPrice * 0.9) {
                        priceSliderInst.setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
                    }
                },
                $counter: $formSliderBlock.find(".MortgageCalc_sum"),
            }
        );
    }

    {
        let $formSliderBlock = $calculatorForm.find(".MortgageCalc_option-price");

        var priceSliderInst = new MortgageSlider(
            $formSliderBlock.find(".MortgageSlider"),
            {
                change: () => {
                    pickupAndRenderBanks();
                },
            },
            {
                slideEventCallback: (/*event, ui*/) => {
                    let selectedFirstPayment = firstPaymentSliderInst.getValue();
                    let selectedPrice = priceSliderInst.getValue();

                    if (selectedFirstPayment >= selectedPrice * 0.9) {
                        firstPaymentSliderInst.setHandlerPosition(Math.round(selectedPrice * 0.9));
                    }
                },
                $counter: $formSliderBlock.find(".MortgageCalc_sum"),
            }
        );
    }

    {
        let $formSliderBlock = $calculatorForm.find(".MortgageCalc_option-duration");

        new MortgageSlider(
            $formSliderBlock.find(".MortgageSlider"),
            {
                change: () => {
                    pickupAndRenderBanks();
                },
            },
            {
                $counter: $formSliderBlock.find(".MortgageCalc_sum"),
                customCounterRender: (v) => {
                    if (v === 1) {
                        return "1 год";
                    } else {
                        return moment.duration(v, "years").humanize();
                    }
                },
            }
        );
    }

    pickupAndRenderBanks();

    $cont
        .on("transitStart", (event) => {
            $cont.find(".b-hypothec__bubble-ico").popover("destroy");
        })
        .on("transitEnd", (event) => {
            $cont.find(".selectpicker").selectpicker("destroy");
            $col.getNiceScroll().remove();

            forms.forEach((index, el) => {
                el.destroy();
            });

            pickupAndRenderBanks = checkBank = MortgageBanklistRenderer = null;
        });
})();
