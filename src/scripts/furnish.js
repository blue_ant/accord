(() => {
    let $cont = $(".l-furnish:first");
    let allSwipers = [];

    let scroller = $cont.find('.b-centered-text:first').niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }
    });

    $cont.find('.b-furnish__mobile-slider').each(function(index, el) {

        var $block = $(el);
        var $popupSliderModal = $block.next();

        let popupSwiper = new Swiper($popupSliderModal.find(".b-popupgallery__slider:first"), {
            slidesPerView: 1,
            centeredSlides: true,
            touchEventsTarget: 'slide',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            nextButton: $popupSliderModal.find('.b-popup-gallery__navbtn--next'),
            prevButton: $popupSliderModal.find('.b-popup-gallery__navbtn--prev'),
            loop: false,
        });

        let swiper = new Swiper($block.find(".swiper-container:first"), {
            direction: 'horizontal',
            loop: false,
        });

        $popupSliderModal.on('shown.bs.modal', function(e) {
            popupSwiper.update();
        });

        allSwipers.push(swiper, popupSwiper);

    });

    $cont.find(".b-popup-gallery").each(function(index, el) {

    });

    $cont.parent().on('transitStart', (event) => {

            $(".b-popup-gallery.in").removeClass('fade').modal("hide");
        })
        .on('transitEnd', (event) => {

            for (var i = allSwipers.length - 1; i >= 0; i--) {
                allSwipers[i].destroy(true, false);
            }

            $cont.find('.b-centered-text:first').getNiceScroll().remove();
            allSwipers = null;
            scroller = null;
        });
})();
