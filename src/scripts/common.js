Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if (a == b) // Or === depending on your needs
        return opts.fn(this);
    else
        return opts.inverse(this);
});

moment.locale('ru');

function getQuartersFromDateRange(start, end) {
    let quarterPoints = [moment(start).unix()];
    let tmpQuarterPoint = new Date(start);

    while (tmpQuarterPoint < end) {
        tmpQuarterPoint.setMonth(tmpQuarterPoint.getMonth() + 3);
        quarterPoints.push(moment(tmpQuarterPoint).unix());
    }

    return quarterPoints;
}

var favoritPlansModalTpl = Handlebars.compile(`
<table class="b-starred-flats-modal__miniplans table">
    <tbody>
        <tr>
        {{#each flats}}
            <td>
                <div class="b-starred-flats-modal__miniplan">
                    <div class="b-starred-flats-modal__plan-hint">Планировка</div>
                    <div class="b-starred-flats-modal__plan-name">{{name}}</div>
                    <div class="b-starred-flats-modal__img-wrap"><img src="{{img}}" alt="" class="b-starred-flats-modal__img">
                    <a href="{{url}}" class="b-starred-flats-modal__img-link b-starred-flats-modal__img-link--go"></a>
                    <a href="#" class="b-starred-flats-modal__img-link b-starred-flats-modal__img-link--remove" data-flat-id="{{id}}"></a>
                    </div>
                </div>
            </td>
        {{/each}}
        </tr>
    </tbody>
</table>
<div class="clearfix">
    <table class="b-starred-flats-modal__table b-starred-flats-modal__table--values table">
        <tbody>
            <tr>
                {{#each flats}}
                <td>
                    <div class="b-starred-flats-modal__plan-val">
                        {{#if_eq area_min area_max}}
                          {{area_min}} m<sup>2</sup>
                        {{else}}
                          {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>
                        {{/if_eq}}
                    </div>
                </td>
                {{/each}}
            </tr>
            <tr>
            {{#each flats}}
                <td>
                    <div class="b-starred-flats-modal__plan-val">{{bedrooms}}</div>
                </td>
            {{/each}}
            </tr>
            <tr>
            {{#each flats}}
                <td>
                    <div class="b-starred-flats-modal__plan-val">+</div>
                </td>
            {{/each}}
            </tr>
            <tr>
            {{#each flats}}
                <td>
                    <div class="b-starred-flats-modal__plan-val">+</div>
                </td>
            {{/each}}
            </tr>
            <tr>
            {{#each flats}}
                <td>
                    <div class="b-starred-flats-modal__plan-val">+</div>
                </td>
            {{/each}}
            </tr>
            <tr>
                {{#each flats}}
                <td>+</td>
                {{/each}}
            </tr>
        </tbody>
    </table>
</div>
`);


function insertFavoritPlansPopup() {
    var isNew = false;
    if (!document.getElementById('starredFlatsModal')) {
        isNew = true;
        $("body:first").after(`
<div class="modal modal-centered fade hidden-xs" id="starredFlatsModal">
    <div class="modal-dialog modal-fluid">
        <div class="modal-content">
            <div class="modal-body" style="position: relative;">
                <div class="b-starred-flats-modal b-starred-flats-modal--empty clearfix">
                    <div class="b-starred-flats-modal__left">
                        <div class="b-starred-flats-modal__title">Избранное</div>
                        <table class="b-starred-flats-modal__table b-starred-flats-modal__table--headings table">
                            <tbody>
                                <tr>
                                    <td>Площадь</td>
                                </tr>
                                <tr>
                                    <td>Количество спален</td>
                                </tr>
                                <tr>
                                    <td>Балкон</td>
                                </tr>
                                <tr>
                                    <td>Система "Умный Дом"</td>
                                </tr>
                                <tr>
                                    <td>Smart-планировка</td>
                                </tr>
                                <tr>
                                    <td>Панорамные окна</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="b-starred-flats-modal__right">
                        <div class="b-starred-flats-modal__right-cont"></div>
                    </div>
                    <div class="b-starred-flats-modal__emptytext">
                        Нет квартир <br>в &laquo;избранном&raquo;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`);

    }

    let $modal = $('#starredFlatsModal');
    let $miniplansWrap = $modal.find('.b-starred-flats-modal__right:first');

    if (isNew) {
        $miniplansWrap.perfectScrollbar({
            suppressScrollY: true,
            useBothWheelAxes: true,
            useKeyboard: false,
        });

        $miniplansWrap.on('click', ".b-starred-flats-modal__img-link--remove", function(event) {
            event.preventDefault();
            showPagePreloader();
            let $inner = $modal.find('.b-starred-flats-modal:first');

            let $miniplan = $(event.currentTarget).parents("td:first");
            let $table = $(".b-starred-flats-modal__table--values:first");
            let colNum = $miniplan.index() + 1;
            let $cols = $table.find(`td:nth-child(${colNum})`);
            let $elsToDelete = $miniplan.add($cols);
            TweenLite.to($elsToDelete, 0.4, {
                alpha: 0,
                scale: 0.9,
                ease: Power1.easeOut,
                onComplete: () => {
                    let planCount = $table.find('tr:first td').length;
                    if (planCount === 1) {
                        $inner.addClass('b-starred-flats-modal--empty');
                    } else {
                        $elsToDelete.remove();
                        $miniplansWrap.perfectScrollbar("update");
                    }

                    let flatId = parseInt(event.currentTarget.dataset.flatId);

                    toggleFavoriteFlat(flatId);
                }
            });
        }).on('click', '.b-starred-flats-modal__img-link', function(event) {
            if (document.getElementsByClassName('l-single-flat').length) {
                event.preventDefault();
                showPagePreloader();
                window.location.href = event.currentTarget.href;
            }
        });
    }

    $modal.on('shown.bs.modal', function(event) {
        $miniplansWrap.perfectScrollbar("update");
    });

    $(window).on(`resize orientationchange`, function(event) {
        if ($modal.data('bs.modal') && $modal.data('bs.modal').isShown) {
            if ($(window).width() < 768) {
                $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
            } else {
                $miniplansWrap.perfectScrollbar("update");
            }
        }
    });

    return {
        miniplansWrap: $miniplansWrap,
        modal: $modal,
    };
}

function refreshFavoritPlansList(cb) {

    $.ajax({
            url: 'http://akkord-smart.ru/apartments/?ajax=1&get_typicals=1&get_favorites=1',
        })
        .done(function(data) {
            let $modal = $('#starredFlatsModal');
            let $inner = $modal.find('.b-starred-flats-modal:first');
            let $stars = $(".b-single-flat__title-star[data-flat-id]:first, .b-flatcard__iconbtn[data-flat-id]");
            let ids = [];

            for (var i = data.typicals.length - 1; i >= 0; i--) {
                ids.push(data.typicals[i].id + "");
            }

            if (data.typicals.length) {
                $stars
                    .removeClass('b-single-flat__title-star--active b-flatcard__iconbtn--active')
                    .filter((index, el) => {
                        return _.includes(ids, el.dataset.flatId);
                    }).each((index, el) => {
                        let className = el.classList.item(0);
                        el.classList.add(className + "--active");
                    });

                let markup = favoritPlansModalTpl({ flats: data.typicals });
                $modal.find('.b-starred-flats-modal__right-cont:first').html(markup);
                $inner.removeClass('b-starred-flats-modal--empty');

            } else {
                $inner.addClass('b-starred-flats-modal--empty');
                $stars.removeClass('b-single-flat__title-star--active b-flatcard__iconbtn--active');
            }
            if (cb) {
                cb();
            }

        });
}

function toggleFavoriteFlat(id, cb) {
    showPagePreloader();
    $.ajax({
            url: '/apartments/?favorite=' + id,
        })
        .done(function(data) {
            refreshFavoritPlansList();
            hidePagePreloader();
            if (cb) {
                cb(data);
            }
        });
}

function genplanShow() {
    let genplan = document.getElementById('genplan');
    let track = genplan.getElementsByClassName('b-genplan__track')[0];
    pullGenplanImg();
    TweenLite.to(genplan, 0.4, {
        autoAlpha: 1,
        onComplete: () => {

            TweenLite.to(track, 0.4, { left: "0%", ease: Power1.easeInOut });

        }
    });

}

function genplanHide() {
    let genplan = document.getElementById('genplan');
    let track = genplan.getElementsByClassName('b-genplan__track')[0];
    TweenLite.to(track, 0.4, {
        left: "100%",
        ease: Power1.easeInOut,
        onComplete: () => {
            TweenLite.to(genplan, 0.4, { autoAlpha: 0 });
        }
    });

}

function insertGenplanModal() {
    if (!document.getElementById('genplan')) {
        $("body:first").after(`
<div class="b-genplan" id="genplan">
    <div class="b-genplan__inner">
    <div class="b-genplan__track">
        <div class="b-genplan__img-wrap">
            <img src="/img/genplan/genplan.jpg" alt="" class="b-genplan__img">
            <a href="#" class="b-genplan__overlink b-genplan__overlink--1A" data-house-num-title="1A" data-house="11">Корпус <b>1A</b></a>
            <div class="b-genplan__overpic b-genplan__overpic--1A"></div>
            <a href="#" class="b-genplan__overlink b-genplan__overlink--2A" data-house-num-title="2A" data-house="13">Корпус <b>2A</b></a>
            <div class="b-genplan__overpic b-genplan__overpic--2A"></div>
            <a href="#" class="b-genplan__overlink b-genplan__overlink--1B" data-house-num-title="1B" data-house="12">Корпус <b>1B</b></a>
            <div class="b-genplan__overpic b-genplan__overpic--1B"></div>
            <a href="#" class="b-genplan__overlink b-genplan__overlink--2B" data-house-num-title="2B" data-house="14">Корпус <b>2B</b></a>
            <div class="b-genplan__overpic b-genplan__overpic--2B"></div>
        </div>
        <div class="b-windrose b-windrose--genplan hidden-xs"></div>
        <div class="b-genplan__show-btn btn btn-sm btn-pale" onclick="genplanHide();"></div>
    </div>
    </div>
    <div class="b-genplan__close-btn" onclick="genplanHide();"></div>


</div>
`);
        $(document).on('click', '[data-house-num-title]', function(event) {

            event.preventDefault();
            let link = event.currentTarget;
            let valId = link.dataset.house;
            let house = link.dataset.houseNumTitle;

            let $housesInputs = $("[name='house\[\]']");

            link.classList.toggle("b-genplan__overlink--active");

            let isChecked = link.classList.contains('b-genplan__overlink--active');

            $(`[name='house\[\]'][value='${valId}']:first`).attr('checked', isChecked).trigger('change');
            populateGenplanHousesBtns();
        });

        $(window).on(`resize orientationchange`, _.debounce(() => {

            pullGenplanImg();

        }, 100));
    }
}

function pullGenplanImg() {
    let $window = $(window);

    let $genplanBlock = $("#genplan");
    let $wrap = $genplanBlock.find('.b-genplan__img-wrap:first');

    if ($window.width() < 768) {
        TweenLite.set($wrap, {
            clearProps: "all",
        });

        return false;
    }

    let $track = $genplanBlock.find('.b-genplan__inner:first');
    let $img = $genplanBlock.find('.b-genplan__img:first');

    let wrapWidth = $track.width();
    let wrapHeight = $window.height();

    let imgWidth = $img.width();
    let imgHeight = $img.height();

    if (wrapWidth / imgWidth > wrapHeight / imgHeight) {
        TweenLite.set($img, {
            width: wrapWidth,
            height: "auto",
        });
    } else {
        TweenLite.set($img, {
            height: wrapHeight,
            width: "auto",
        });
    }

    let newImgHeight = $img.height();
    let newImgWidth = $img.width();

    TweenLite.set($wrap, {
        marginLeft: newImgWidth / -2,
        marginTop: newImgHeight / -2,
    });

    return true;

}

function populateGenplanHousesBtns() {
    let $checkedLinks = $(`.b-genplan__overlink--active`);
    let $btn = $('.b-flats-filter__round-btn--build');
    let $showResultBtn = $(`.b-genplan__show-btn:first`);

    if ($checkedLinks.length) {

        let btnStr = "";
        $checkedLinks.each(function(index, el) {
            let prefix = "";
            if (index !== 0) {
                prefix = ", ";
            }
            btnStr += (prefix + el.dataset.houseNumTitle);
        });

        $btn
            .addClass('b-flats-filter__round-btn--wo-icon')
            .text(btnStr);

        $showResultBtn.addClass('b-genplan__show-btn--active');
    } else {
        $btn
            .removeClass('b-flats-filter__round-btn--wo-icon')
            .text("");

        $showResultBtn.removeClass('b-genplan__show-btn--active');
    }
}

function preserveFiltersParams(opts) {
    let $inputs = opts.inputs;
    if (opts.skip) {
        $inputs = $inputs.filter(function(index, element) {
            return (_.indexOf(opts.skip, element.name) === -1);

        });
    }
    let inputsArr = $inputs.serializeArray();

    localStorage.setItem(opts.storageID, JSON.stringify(inputsArr));
}

function restoreFiltersParams(opts) {
    let $form = opts.form;
    let storageID = opts.storageID;

    let preservedFiltersData = localStorage.getItem(storageID);
    if (!preservedFiltersData) return;

    let statesArr = JSON.parse(preservedFiltersData);
    statesArr = _.groupBy(statesArr, "name");

    let areaSliderData = [0, 0];
    let realeseSliderData = [];

    for (let k in statesArr) {

        if (opts.skip && _.indexOf(opts.skip, k) !== -1) {
            continue;
        }
        let $inputsByName = $form.find(`[name="${k}"]`);
        for (var i = statesArr[k].length - 1; i >= 0; i--) {
            let storedVal = statesArr[k][i].value;

            if ($inputsByName.attr('type') === "checkbox") {
                let $inp = $inputsByName.filter(`[value='${storedVal}']:first`);
                $inp.attr('checked', true);

            } else {
                let name = $inputsByName.attr('name');

                if (name === "area_from") {
                    areaSliderData[0] = parseInt(storedVal);

                } else if (name === "area_to") {
                    areaSliderData[1] = parseInt(storedVal);

                } else if (name === "finish_from") {
                    realeseSliderData[0] = parseInt(storedVal);
                } else if (name === "finish_to") {
                    realeseSliderData[1] = parseInt(storedVal);
                }

                $inputsByName.val(storedVal);
            }
        }

    }

    if (areaSliderData[0]) {
        $form.find('[data-area-slider]:first').bootstrapSlider("setValue", areaSliderData, true);
    }

    if (realeseSliderData[0]) {
        $form.find('[data-realese-slider]:first').bootstrapSlider("setValue", realeseSliderData, true);
    }

    let $genplanLinks = $(".b-genplan__overlink");

    $form.find("[name='house\[\]']").each((index, el) => {
        if (el.checked) {
            $genplanLinks.filter(`[data-house="${el.value}"]`).addClass('b-genplan__overlink--active');
        }
    });

    populateGenplanHousesBtns();
}

function showMainMenu() {
    let $window = $(window);
    let $menu = $("#mainMenu");
    let $ball = $menu.find('.b-menu__ball:first');
    let $mobileSocial = $menu.find('.b-menu__soc--mobile:first');
    let $inner = $menu.find('.b-menu__inner:first');
    let tl = new TimelineLite();
    let $closeBtn = $menu.find('.b-menu__close-btn:first');
    let h = $window.height();
    let w = $window.width();

    let hypot = (Math.sqrt(w * w + h * h)) * 2;

    tl
        .set($menu, { visibility: "visible", background: "none", pointerEvents: "none" })
        .add('animeStart')
        .to($ball, 0.6, {
            width: hypot,
        })
        .from($closeBtn, 0.3, {
            rotation: 360,
            opacity: 0,
        }, "animeStart")
        .to([$inner, $mobileSocial], 0.2, {
            opacity: 1
        })
        .set($menu, { clearProps: "background,pointerEvents" });
}

function hideMainMenu() {

    let $menu = $("#mainMenu");
    let $ball = $menu.find('.b-menu__ball:first');
    let $mobileSocial = $menu.find('.b-menu__soc--mobile:first');
    let $inner = $menu.find('.b-menu__inner:first');
    let $closeBtn = $menu.find('.b-menu__close-btn:first');

    let tl = new TimelineLite();
    tl
        .set($menu, { background: "none", pointerEvents: "none" })
        .add("animeStart")
        .to([$inner, $mobileSocial], 0.2, {
            opacity: 0
        }, "animeStart")
        .to($closeBtn, 0.3, {
            rotation: 360,
            opacity: 0,
        }, "animeStart")
        .to($ball, 0.6, {
            width: 0,
        })

        .set([$menu, $ball, $inner, $mobileSocial, $closeBtn], { clearProps: "all" });

}

function setActiveMainMenuLink() {
    let url = window.location.href;
    let $links = $('.b-menu__leaf');
    $links
        .removeClass('b-menu__leaf--active').each(function(index, el) {

            if (_.includes(url, el.href)) {
                el.classList.add("b-menu__leaf--active");
                return false;
            }
        });
}

function showPagePreloader(cb) {
    let preloaderOverlay = document.getElementById("pagePreloader");
    if (preloaderOverlay) {
        let pause = 0.2;
        TweenLite.killTweensOf(preloaderOverlay);
        TweenLite.to(preloaderOverlay, 0.2, {
            autoAlpha: 1,
            delay: pause,
            onComplete: () => {
                if (cb) {
                    cb();
                }
            }
        });

    }

}

function hidePagePreloader(cb) {

    if (document.readyState !== "complete" || window.accordAppStates.isPreloaderLockedOn) {
        return false;
    }

    let preloaderOverlay = document.getElementById("pagePreloader");
    if (preloaderOverlay) {
        TweenLite.killTweensOf(preloaderOverlay);
        TweenLite.to(preloaderOverlay, 0.2, {
            autoAlpha: 0,
            onComplete: () => {
                if (cb) {
                    cb();
                }
            }
        });
    }
}

function clearFilterForm(id) {
    let $form = $(id);
    $form.find('input[type="checkbox"]').prop('checked', false);
    //localStorage.removeItem("flatsFiltersFormStorage");
    $form.find('.b-multycheckbox__input').each(function(index, el) {
        let $inp = $(el);
        let possibleValues = $inp.data('possible-values');
        $inp.val(possibleValues[0]);
    });

    $form.find('[data-slider-value]').each(function(index, el) {
        let $rangeSlider = $(el);
        let initialValues = $rangeSlider.data("slider-value");
        $rangeSlider.bootstrapSlider("setValue", initialValues, true);
    });

    let $activeHouseLinks = $("#genplan").find(".b-genplan__overlink--active");

    if ($activeHouseLinks.length) {
        $activeHouseLinks.trigger('click');
    } else {
        $form.find('input:first').trigger('change');
    }

}

function switchScreen(opts, cb) {
    $(".b-aside-form__backbtn").trigger('click');
    hideMainMenu();
    let $preloaderOverlay = $("#pagePreloader");
    showPagePreloader();
    $.ajax({
        url: opts.path,
        dataType: 'html',
    }).always((data) => {
        let $cont = $("#pageContainer");
        let $oldView = $cont.find('.l-main__inner');
        let $newDoc;
        if (data.responseText) {
            $newDoc = $(data.responseText);
        } else {
            $newDoc = $(data);
        }

        let $newView = $newDoc.find(".l-main__inner:first");

        let pageTypesMap = {
            "l-404": {
                isTransitWithClouds: false,
            },
            "b-homepage-slides": {
                isTransitWithClouds: false,
                isHeaderWhite: true,
            },
            "l-about": {
                isTransitWithClouds: true,
            },

            "l-action": {
                isTransitWithClouds: false,
            },
            "l-actions": {
                isTransitWithClouds: false,
            },
            "l-article": {
                isTransitWithClouds: false,
            },
            "l-buy": {
                isTransitWithClouds: true,
            },
            "l-contacts": {
                isTransitWithClouds: true,
            },
            "l-diary": {
                isTransitWithClouds: false,
                dontWaitFullLoad: true,
            },
            "l-docs": {
                isTransitWithClouds: false,
            },
            "l-sitemap": {
                isTransitWithClouds: false,
            },
            "l-single-flat": {
                isTransitWithClouds: false,
                preserveFilters: true,
            },
            "l-flats": {
                isTransitWithClouds: false,
                preserveFilters: true,
            },

            "l-storerooms": {
                isTransitWithClouds: false,
                preserveFilters: true,
            },

            "l-furnish": {
                isTransitWithClouds: false,
            },
            "l-gallery": {
                isTransitWithClouds: false,
            },
            "l-hypothec": {
                isTransitWithClouds: false,
            },
            "l-infras": {
                isTransitWithClouds: true,
            },
            "l-news": {
                isTransitWithClouds: false,
            },
            "l-baby": {
                isTransitWithClouds: false,
            },
        };

        let wrapperClassName = $newView.find(' > div').not('.hostcmsPanel').get(0).classList.item(0);
        let oldWrapperClassName = $oldView.find(' > div').not('.hostcmsPanel').get(0).classList.item(0);

        let oldPageOpts = pageTypesMap[oldWrapperClassName];
        let pageOpts = pageTypesMap[wrapperClassName];
        if (!pageOpts.preserveFilters) {
            localStorage.removeItem("flatsFiltersFormStorage");
        }
        document.title = $newDoc.filter('title:first').text();

        TweenLite.set($newView, { alpha: 0 });

        $newView.waitForImages({
            finished: function() {
                $cont.append($newView);
                $oldView.trigger('transitStart');

                //TweenMax.killAll(false, false, true, false);

                let tl = new TimelineLite();
                if ((pageOpts.isTransitWithClouds || oldPageOpts.isTransitWithClouds) &&
                    !window.accordAppStates.isCloudsAnimeDisabled &&
                    $(window).width() >= 758) {
                    hidePagePreloader();
                    let $cloudsBg = $("#cloudsOverlay");
                    TweenMax.killChildTweensOf($cloudsBg);
                    tl
                        .set($cloudsBg, { display: "block" })
                        .add("crossfade")

                        .to($cloudsBg, 1.2, {
                            y: "-100%",
                        }, "crossfade")

                        .to($oldView, 1, {
                            alpha: 0,
                            delay: 1

                        }, "crossfade")
                        .call(() => {
                            window.requestAnimationFrame(() => {
                                if (pageOpts.isHeaderWhite) {
                                    document.querySelector(".l-topbar").classList.add("l-topbar--white");
                                } else {
                                    document.querySelector(".l-topbar").classList.remove("l-topbar--white");
                                }
                            });
                        })
                        .add("cloudFadeout")
                        .to($cloudsBg, 1.2, {
                            opacity: 0.7,
                            y: "0%",
                            ease: Power1.easeIn
                        }, "cloudFadeout")
                        .to($newView, 0.6, {
                            css: { alpha: 1 },

                        }, "cloudFadeout")

                        .set($cloudsBg, {
                            clearProps: "all",
                            onComplete: () => {
                                window.scrollTo(0, 0);
                                $oldView.trigger('transitEnd').remove();
                                setActiveMainMenuLink();

                                if (cb) cb();
                            }
                        });

                } else {
                    window.requestAnimationFrame(() => {
                        if (pageOpts.isHeaderWhite) {
                            document.querySelector(".l-topbar").classList.add("l-topbar--white");
                        } else {
                            document.querySelector(".l-topbar").classList.remove("l-topbar--white");
                        }
                    });
                    hidePagePreloader();
                    tl.add("crossfade")
                        .to($newView, 1, {
                            css: { alpha: 1 },

                        }, "crossfade")
                        .to($oldView, 1, {
                            alpha: 0,
                            onComplete: () => {
                                $oldView.trigger('transitEnd').remove();
                                setActiveMainMenuLink();

                                if (cb) cb();
                            }
                        }, "crossfade");
                }
            },

            waitForAll: true,
        });

        window.accordAppStates.isCloudsAnimeDisabled = false;

    });
}

window.accordAppStates = {
    isCloudsAnimeDisabled: false,
};

$(document).on('click', ".b-menu__leaf", function(event) {
        window.accordAppStates.isCloudsAnimeDisabled = true;
    }).on('click', '.b-multycheckbox__btn', function(event) {
        event.preventDefault();
        let btn = event.currentTarget;
        let $inp = $(btn.parentNode.getElementsByTagName('input')[0]);
        let possibleValues = $inp.data("possible-values");
        for (var i = possibleValues.length - 1; i >= 0; i--) {
            possibleValues[i] = possibleValues[i] + "";
        }
        let currentVal = $inp.val() + "";
        let currentValIndex = _.indexOf(possibleValues, currentVal);
        let newVal;
        if (currentValIndex === possibleValues.length - 1) {
            newVal = possibleValues[0];
        } else {
            newVal = possibleValues[currentValIndex + 1];
        }
        $inp.val(newVal).trigger('change');

    })
    .ready(function() {
        $("#pagePreloader").on('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        });
    }).on('click', '[data-aside-menu]', (event) => {
        event.preventDefault();
        $(".b-aside-form__backbtn").trigger('click');
        let targetMenu = document.getElementById(event.currentTarget.dataset.asideMenu);
        let targetXposition = "100%";
        if (targetMenu.classList.contains("b-aside-form--right")) {
            targetXposition = "-100%";
        }
        TweenLite.to(targetMenu, 0.45, { x: targetXposition, ease: Power1.easeInOut });

    }).on('click', '[data-dismiss-aside-menu]', function(event) {
        event.preventDefault();
        let targetMenu;
        let link = event.currentTarget;

        if (link.dataset.dismissAsideMenu) {
            targetMenu = document.getElementById(event.currentTarget.dataset.asideMenu);
        } else {
            targetMenu = link.parentNode;
        }
        TweenLite.to(targetMenu, 0.45, {
            x: '0%',
            ease: Power1.easeInOut,
            onComplete: () => {
                let $form = $(targetMenu).find("form:first");
                let validator = $form.trigger('reset').data("validator");
                if (validator) {
                    validator.resetForm();
                }

                TweenLite.set([$form, targetMenu.getElementsByClassName('b-aside-form__header')], { clearProps: "all" });
                $form.next(".b-aside-form__success-text").remove();
            }
        });
    }).on('click', '.b-nav__burger', function(event) {
        showMainMenu();
    }).on('click', '.b-flats__controls-btn', function(event) {
        event.preventDefault();
        event.currentTarget.nextElementSibling.classList.add("l-flats__filters--active");

    }).on('click', '.b-flats-filter__close', function(event) {
        event.preventDefault();
        event.currentTarget.parentNode.classList.remove("l-flats__filters--active");
    });

$(window).on('load', (event) => {
    event.preventDefault();
    let app = Sammy();
    app.run();
    app.get('(.*)', function() {
        switchScreen({
            path: this.params.splat,
        });
        try {
            ga('set', 'page', this.path);
            ga('send', 'pageview');
            yaCounter29113380.hit("http://" + window.location.hostname + this.path);
        } catch (err) {
            console.log(err);
        }
    });

    $(".b-menu__close-btn:first").on('click', () => {
        hideMainMenu();
    });

    setActiveMainMenuLink();

    $("#cloudsOverlay").on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });

    hidePagePreloader();

    setTimeout(() => {
        let $preloaderOverlay = $("#pagePreloader");
        $preloaderOverlay.addClass('b-page-preloader--transparent');
    }, 0.9);

    $(".l-footer").on('selectstart', function(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });
    new InteractiveForm({ el: "#callMeForm", successTextClass: "b-aside-form__success-text" });

});

function fireTrackersOnSubmit(path) {
    try {
        switch (path) {
            case '/ajax/form-callbackmain/':
                yaCounter29113380.reachGoal('Naglavnoi');
                ga('send', 'event', 'Naglavnoi', 'click1', 'forma1');
                break;
            case "/ajax/form-call/":
                yaCounter29113380.reachGoal('Zakaz_zvonka');
                ga('send', 'event', 'Zakaz_zvonka', 'click2', 'forma2');
                break;
            case "/ajax/form-feedback/":
                yaCounter29113380.reachGoal('Napichi_nam');
                ga('send', 'event', 'Napichi_nam', 'click3', 'forma3');
                break;
            case "/ajax/form-getprice2/":
                yaCounter29113380.reachGoal('Planirovki_usloviya');
                ga('send', 'event', 'Planirovki_usloviya', 'click4', 'forma4');
                break;
            case "/ajax/form-getprice/":
                yaCounter29113380.reachGoal('Planirovki_stoimost');
                ga('send', 'event', 'Planirovki_stoimos', 'click5', 'forma5');
                break;
            case "/ajax/form-caroder/":
                yaCounter29113380.reachGoal('Zakaz_transport');
                ga('send', 'event', 'Zakaz_transport', 'click6', 'forma6');
                break;
            case "/ajax/form-mortgage/":
                yaCounter29113380.reachGoal('Ipoteka');
                ga('send', 'event', 'Ipoteka', 'click7', 'forma7');
                break;

            default:
                // code block
        }
    } catch (e) {
        console.log(e);
    }
}

/*=require ./includes/* */
