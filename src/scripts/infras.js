(() => {
    let $cont = $(".l-infras--internal:first").parent();


    $cont.find(".b-taglist__link").on('click', (event) => {
        event.preventDefault();
        let $tag = $(event.currentTarget);
        let $pins = $(".b-contacts-map__pin");

        $tag.toggleClass('b-taglist__link--active');

        let $activeTags = $(".b-taglist__link--active");
        if (!$activeTags.length) {
        	$pins.fadeIn();
        }else{
            let activePinsGroups = [];
        	$activeTags.each(function(index, el) {
                activePinsGroups.push(el.dataset.pins);
        	});
        	
            let $activePins = $pins.filter(function(index,el) {
                return _.includes(activePinsGroups, el.dataset.pinGroup);
            });

            let $nonActivePins = $pins.filter(function(index,el) {
                return !_.includes(activePinsGroups, el.dataset.pinGroup);
            });

        	$activePins.fadeIn();
            $nonActivePins.fadeOut();
        }

    });

})();
