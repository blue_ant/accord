(() => {
    let $cont = $(".l-sitemap");
    let $col = $cont.find('.b-centered-text');
    $col.niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }
    });
})();
