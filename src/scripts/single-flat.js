(() => {
    insertGenplanModal();
    var handlersID = "singleFlatPage" + _.random(1, 10000);

    var starredFlatsModal = insertFavoritPlansPopup();

    var $starredFlatsModal = starredFlatsModal.modal;
    var $miniplansWrap = starredFlatsModal.miniplansWrap;

    refreshFavoritPlansList();

    var $genplanModal = $("#genplanModal");
    var $conts = $(".l-single-flat");
    var $cont = $conts.eq($conts.length - 1).parent();
    var $flatsListCont = $cont.find('#flastListTableContainer');
    var $schemeModal = $("#floorSchemeModal");
    var $3dModal = $('#3d-modal');

    $('[data-3d]').on('click', function(e) {
        let $this = $(this);
        let href  = $this.data('3d');

        e.preventDefault();

        setTimeout(function() {
            $3dModal.find('[data-modal-3d]').html('<iframe style="width: 100%; height: 100%" seamless="seamless" src="/3d/' + href + '"></iframe>');
            $3dModal.modal('show');
        }, 200);
    });

    var scroller1 = $cont.find('.b-single-flat:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#singFlatPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    $(document).on('click.' + handlersID, '.b-flats-filter__apply-btn', function(event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    });

    var rangeSlider1 = $cont.find('#singleFlatAreaRangeInput:first').bootstrapSlider({
        tooltip: 'hide'
    }).on("slide", function(event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#singleFlatAreaMin").html(min);
        $("#singleFlatAreaMax").html(max);

        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    let $releaseSliderInp = $cont.find('#singleFlatCompletenessRangeInput:first');
    let firstRelease = new Date($releaseSliderInp.data("slider-min") * 1000);
    let lastRelease = new Date($releaseSliderInp.data("slider-max") * 1000);

    let quarterPoints = getQuartersFromDateRange(firstRelease, lastRelease);

    let interval = 31 * 3 * 24 * 60 * 60;
    let rangeSlider2 = $releaseSliderInp.bootstrapSlider({
        tooltip: 'hide',
        ticks: quarterPoints,
        ticks_snap_bounds: interval,
        min: quarterPoints[0],
        max: _.last(quarterPoints),
        step: interval,
    }).on("slide", function(event) {
        var min = event.value[0];
        var max = event.value[1];
        $cont.find('[name="finish_from"]:first').val(min);
        $cont.find('[name="finish_to"]:first').val(max);
        var fmtTpl = 'Qкв YYYY';
        if (Date.now() / 1000 > min) {
            $("#singleFlatCompletenessDateMin").text("Готово");
        } else {
            $("#singleFlatCompletenessDateMin").text(moment.unix(min).format(fmtTpl));
        }

        $("#singleFlatCompletenessDateMax").text(moment.unix(max).format(fmtTpl));
    });

    rangeSlider2.bootstrapSlider("setValue", rangeSlider2.bootstrapSlider("getValue"), true);

    var flatsTableTpl = Handlebars.compile(`

    {{#if flats}}
    <section class="b-flats-list hidden-xs">
        <table class="b-flats-list__table table">
        <caption class="b-flats-list__caption">Квартиры выбранной планировки</caption>
        <tbody>
            {{#each flats}}
            <tr>
                <td class="b-flats-list__td b-flats-list__td--n1">№{{num}}</td>
                <td class="b-flats-list__td">корпус <b class="text-primary">{{house}}</b></td>
                <td class="b-flats-list__td">секция <b class="text-primary">{{section}}</b></td>
                <td class="b-flats-list__td">этаж <b class="text-primary">{{floor}}</b></td>
                <td class="b-flats-list__td">{{#if ../typical.bedrooms}}спален <b class="text-primary">{{../typical.bedrooms}}</b>{{else}}студия{{/if}}</td>
                <td class="b-flats-list__td">общая площадь <b class="text-primary">{{area}} м<sup>2</sup></b></td>
                <td class="b-flats-list__td"><u data-plan-img="{{plan_floor}}" data-plan-section="{{section}}">на этаже</u></td>
                <td class="b-flats-list__td">
                    <div class="b-flats-list__icon b-flats-list__icon--percent{{#if ../typical.balcony}} b-flats-list__icon--active{{/if}}" data-toggle="popover" data-content="{{#if ../typical.balcony}}с&nbsp;балконом{{else}}без балкона{{/if}}"></div>
                </td>
                <td class="b-flats-list__td">
                    Стоимость <b class="text-primary">{{price}} руб.</b>
                </td>
            </tr>
            {{/each}}
        </tbody>
        </table>
    </section>
    <section class="b-mobile-flats-list visible-xs">
        <table class="b-mobile-flats-list__table table">
            <caption class="b-mobile-flats-list__caption">Квартиры выбранной планировки</caption>
            <tbody> {{#each flats}}
                <tr>
                    <td class="b-mobile-flats-list__td">
                        <div class="b-mobile-flats-list__value">№{{num}}</div> квартира</td>
                    <td class="b-mobile-flats-list__td">
                        <div class="b-mobile-flats-list__value">{{house}}</div> корпус </td>
                    <td class="b-mobile-flats-list__td">
                        <div class="b-mobile-flats-list__value">{{floor}}</div> этаж </td>
                    <td class="b-mobile-flats-list__td">
                        <div class="b-mobile-flats-list__value">{{area}} м<sup>2</sup></div> площадь </td>
                    <td class="b-mobile-flats-list__td">
                        <div class="b-mobile-flats-list__icon b-mobile-flats-list__icon--percent{{#if ../typical.balcony}} b-mobile-flats-list__icon--active{{/if}} data-toggle="popover" data-content="{{#if ../typical.balcony}}с&nbsp;балконом{{else}}без балкона{{/if}}"></div>
                    </td>
                </tr>

            <tr>
                <td colspan="10 " class="b-mobile-flats-list__td b-mobile-flats-list__td--price ">
                <div class="b-mobile-flats-list__value">{{price}} руб.</div>
                    Стоимость
                </td>
            </tr>
                {{/each}}
            </tbody>
        </table>
    </section>
    {{else}}

    <h3 style="margin-top: 86px;">Нет квартир по вашим параметрам.</h3>
    <h5>Попробуйте изменить параметры фильтров.</h5>

    {{/if}}

    <div style="position:absolute;height:0;width:0;visibility:hidden;background:{{#each flats}} url('{{plan_floor}}'),{{/each}}transparent;"></div>
    `);

    $flatsListCont.on('mousedown.' + handlersID, "[data-plan-img]", function(event) {
        event.preventDefault();
        var link = event.currentTarget;
        $schemeModal.find('.b-floor-scheme__img:first').css('background-image', "url(" + link.dataset.planImg + ")");
        $schemeModal.find('.b-floor-scheme__title:first').html("\u0421\u0435\u043A\u0446\u0438\u044F <span class='text-primary'>" + link.dataset.planSection + "</span>");
    }).on('click.' + handlersID, '[data-plan-img]', function(event) {
        event.preventDefault();
        setTimeout(function() {
            $schemeModal.modal("show");
        }, 200);
    });

    var flatID = parseInt(window.location.href.split("typical-")[1]);
    var $filterForm = $cont.find("#formFilterSingleApartment");

    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage"
    });

    $filterForm.find("input").on("change", _.debounce(function() {
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function(event) {
        event.preventDefault();
        showPagePreloader();
        var $nonEmptyInputs = $filterForm.find("input").filter(function(index, element) {
            return $(element).val() !== "";
        });

        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage"
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }

        $.ajax({
            url: '/apartments/?ajax=1&get_flats=' + flatID + urlParams

        }).done(function(data) {
            $flatsListCont.find(".b-flats-list__icon,.b-mobile-flats-list__icon").popover("destroy");

            var markup = flatsTableTpl(data);
            $flatsListCont.html(markup);

            hidePagePreloader();

            $flatsListCont.find(".b-flats-list__icon,.b-mobile-flats-list__icon").popover({
                trigger: "hover",
                placement: "top"
            });
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.find('.b-single-flat__icon').popover({
        html: true,
        trigger: "hover"
    });

    $cont.find(".b-single-flat__title-star:first").on('click', function(event) {
        event.preventDefault();
        toggleFavoriteFlat(this.dataset.flatId);
    });

    var $asideForm = $("#form7").eq(0);
    var getFlatForm = new InteractiveForm({ el: $asideForm, successTextClass: "b-aside-form__success-text" });

    let flatPlanSlider = new FlatplanSlider(".FlatplanSlider");

    $cont.on('transitStart', function(event) {
        genplanHide();
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document, $flatsListCont]).off("." + handlersID);
        $cont.find('.b-single-flat__icon,.b-flats-list__icon,.b-mobile-flats-list__icon').popover('destroy');
    }).on('transitEnd', function(event) {

        

        rangeSlider1.bootstrapSlider('destroy');
        rangeSlider2.bootstrapSlider('destroy');

        getFlatForm.destroy();
        getFlatForm = null;

        // TODO: find a reason of the error "scroller1.remove is not a function"
        try{
            scroller1.remove();
            scroller2.remove();
        }catch(err){
            console.log(err);
        }

        // TODO: fix the possible missing of titles
        try{
            flatPlanSlider.destroy();
        }catch(err){
            console.log(err);
        }
    });
})();
