(function() {

    insertGenplanModal();
    
    var starredFlatsModal = insertFavoritPlansPopup();

    var $starredFlatsModal = starredFlatsModal.modal;
    var $miniplansWrap = starredFlatsModal.miniplansWrap;

    refreshFavoritPlansList();

    var $genplanModal = $("#genplanModal");
    var $cont = $(".l-flats:first").parent();
    var scroller1 = $cont.find('.b-flats:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#allFlatsPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    $(document).on('click.flatspage', '.b-flats-filter__apply-btn', function(event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    }).on('click.flatspage', '.b-flatcard__star .b-flatcard__iconbtn', function(event) {
        event.preventDefault();
        var link = event.currentTarget;
        toggleFavoriteFlat(link.dataset.flatId);
    });

    let rangeSlider1 = $cont.find('#areaRangeInput:first').bootstrapSlider({
        tooltip: 'hide'
    }).on("slide", function(event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#areaMin").html(min);
        $("#areaMax").html(max);

        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    

    let $releaseSliderInp = $cont.find('#completenessRangeInput:first');
    let firstRelease = new Date($releaseSliderInp.data("slider-min") * 1000);
    let lastRelease = new Date($releaseSliderInp.data("slider-max") * 1000);


    let quarterPoints = getQuartersFromDateRange(firstRelease, lastRelease);

    let interval = 31*3*24*60*60;
    let rangeSlider2 = $releaseSliderInp.bootstrapSlider({
        tooltip: 'hide',
        ticks:quarterPoints,
        ticks_snap_bounds: interval,
        min:quarterPoints[0],
        max:_.last(quarterPoints),
        step: interval,
    }).on("slide", function(event) {
        var min = event.value[0];
        var max = event.value[1];
        $cont.find('[name="finish_from"]:first').val(min);
        $cont.find('[name="finish_to"]:first').val(max);
        var fmtTpl = 'Qкв YYYY';
        if (Date.now() / 1000 > min) {
            $("#completenessDateMin").text("Готово");
        } else {
            $("#completenessDateMin").text(moment.unix(min).format(fmtTpl));
        }

        $("#completenessDateMax").text(moment.unix(max).format(fmtTpl));
    });

    rangeSlider2.bootstrapSlider("setValue", rangeSlider2.bootstrapSlider("getValue"), true);

    const flatCellTpl = Handlebars.compile(`

{{#if typicals}}
{{#each typicals}}
<div class="b-flats__cell col-xs-12 col-sm-6 col-md-4">
   <div class="b-flats__cell-inner">
       <div class="b-flatcard hidden-xs">
           <div class="b-flatcard__row">
               <div class="b-flatcard__cell">
                   <div class="va">
                       <div class="va-item b-flatcard__title">{{name}}</div>
                       <div class="va-item">
                           <div class="b-flatcard__star">
                               <div class="va-item">
                                   <a href="#" data-flat-id="{{id}}" class="b-flatcard__iconbtn{{#if favorite}} b-flatcard__iconbtn--active{{/if}}"> <img src="/img/star-primary.svg" class="b-flatcard__icon" alt=""> </a>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <div class="b-flatcard__row b-flatcard__row--middle">
               <div class="b-flatcard__cell">
                   <a href="{{url}}" class="b-flatcard__img" style="background-image: url({{img}});"></a>
               </div>
           </div>
           <div class="b-flatcard__row">
               <div class="b-flatcard__cell">
                   <div class="va">
                       <div class="va-item b-flatcard__footage">
                        {{#if_eq area_min area_max}}
                          {{area_min}} m<sup>2</sup>
                        {{else}}
                          {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>
                        {{/if_eq}}
                       </div>
                       <div class="va-item">
                           <a href="#" class="b-flatcard__iconbtn" data-toggle="popover" data-content="{{#if balcony}}с&nbsp;балконом{{else}}без&nbsp;балкона{{/if}}"> <img src="/img/balcony-primary.svg" class="b-flatcard__icon{{#if balcony}} b-flatcard__icon--active{{/if}}" alt=""> </a>
                       </div>
                      
                   </div>
               </div>
           </div>
       </div>
       <div class="b-mobile-flatcard container-fluid visible-xs">
           <div class="row">
               <div class="col-xs-7"> <a href="{{url}}"><img src="{{img}}" alt="" class="b-mobile-flatcard__img img-responsive center-block"></a> </div>
               <div class="col-xs-5">
                   <div class="b-mobile-flatcard__title">{{name}}</div>
                   <div class="b-mobile-flatcard__footage">
                   {{#if_eq area_min area_max}}
                      {{area_min}} m<sup>2</sup>
                    {{else}}
                      {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>
                    {{/if_eq}}
                    </div>
                   <div class="b-mobile-flatcard__icons">
                       <div class="b-mobile-flatcard__iconwrap text-center">
                           <a href="#" class="b-mobile-flatcard__iconbtn" data-toggle="popover" data-content="{{#if balcony}}с&nbsp;балконом{{else}}без&nbsp;балкона{{/if}}"> <img src="/img/balcony-primary.svg" class="b-mobile-flatcard__icon{{#if balcony}} b-mobile-flatcard__icon--active{{/if}}" alt=""> </a>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
{{/each}}
{{else}}

<div class="b-flats__empty">
    <div class="b-flats__empty-inner">
        <img src="/img/cloud.svg" alt="" class="img-responsive">
        <div class="b-flats__empty-header">НЕТ РЕЗУЛЬТАТОВ</div>
        <div class="b-flats__empty-text">Планировки по заданным параметрам не найдены,<br>
пожалуйста измените параметры поиска.</div>
        <img src="/img/crane.svg" alt="" class="b-flats__empty-bootom-img">
    </div>
</div>

{{/if}}

<div class="col-xs-12 hidden-xs" style="padding-top: 20px;">&nbsp;</div>

`);

    var $filterForm = $cont.find("#formFilterApartments");

    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage"
    });

    $filterForm.find("input").on("change", _.debounce(function() {
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function(event) {
        showPagePreloader();
        event.preventDefault();
        var $nonEmptyInputs = $filterForm.find("input").filter(function(index, element) {
            return $(element).val() !== "";
        });

        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage"
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }
        $.ajax({
            url: 'http://akkord-smart.ru/apartments/?get_typicals=1&ajax=1&filter=1' + urlParams

        }).done(function(data) {
            // fix pics urls in development enviroment
            if (window.location.href.indexOf("/flats.html") != -1) {
              console.warn("rewrite json response for dev enviroment usage...");
              data.typicals.forEach((plan) => {
                  plan.img = "http://akkord-smart.ru/" + plan.img;
                  plan.url = "/single-flat.html";
              });
            }
            var markup = flatCellTpl(data);
            var $cellsCont = $cont.find("#flatsCellsContainer");
            $cellsCont.html(markup);

            $cellsCont.find(".b-flatcard__iconbtn,.b-mobile-flatcard__iconbtn").popover({
                trigger: "hover",
                placement: "top"
            });
            hidePagePreloader();
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.on('transitStart', function(event) {
        genplanHide();
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document]).off(".flatspage");
        $cont.find(".b-flatcard__iconbtn,.b-mobile-flatcard__iconbtn").popover("destroy");
    }).on('transitEnd', function(event) {

        rangeSlider1.bootstrapSlider('destroy');
        rangeSlider2.bootstrapSlider('destroy');

        scroller1.remove();
        scroller2.remove();
    });
})();
