(() => {
    $(".b-popup-gallery").appendTo(".l-diary:first");
    let allSwipers = [];
    let $lineSliders = $(".b-gallery__img-slider");
    let $popupSliders = $(".b-popupgallery__slider");

    let primeSwiper = new Swiper('.b-gallery__primeslider', {
        effect: 'fade',
        fade: {
            crossFade: true
        },
        nextButton: '.b-gallery__mobswitcher-navbtn--next',
        prevButton: '.b-gallery__mobswitcher-navbtn--prev',
        onlyExternal: true,
        onTransitionStart: function(swiper) {
            var $titles = $('.b-gallery__title');
            var num = swiper.realIndex;
            $titles.
            filter('.b-gallery__title--active:first')
                .removeClass('b-gallery__title--active');
            $titles.eq(num).addClass('b-gallery__title--active');

        },
        onInit: function(swiper) {
            $(".b-gallery__title ").on('click', function(event) {
                event.preventDefault();
                swiper.slideTo($(this).index());
            });
        }
    });

    allSwipers.push(primeSwiper);

    $lineSliders.each(function(index, el) {
        let $slider = $(el);
        let $controls = $slider.next();
        let $popupSlider = $popupSliders.eq(index);
        let $popupSliderModal = $popupSlider.parent().parent();

        let popupSwiper = new Swiper($popupSliders.eq(index), {
            slidesPerView: 1,
            centeredSlides: true,
            touchEventsTarget: 'slide',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            nextButton: $popupSliderModal.find('.b-popup-gallery__navbtn--next'),
            prevButton: $popupSliderModal.find('.b-popup-gallery__navbtn--prev'),
            loop: false,
        });

        let swiper = new Swiper($slider, {
            pagination: $controls.find('.b-gallery__pager:first'),
            slidesPerView: 'auto',
            centeredSlides: true,
            paginationClickable: true,
            paginationType: 'fraction',
            touchEventsTarget: 'wrapper',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            nextButton: $controls.find('.b-gallery__navbtn--next'),
            prevButton: $controls.find('.b-gallery__navbtn--prev'),
            paginationFractionRender: function(swiper, currentClassName, totalClassName) {
                return '<div class="b-gallery__counter b-gallery__counter--current ' + currentClassName + ' "></div><div class="b-gallery__counter b-gallery__counter--total ' + totalClassName + ' "></div>';
            },
            onTransitionEnd: (swiper) => {
                popupSwiper.slideTo(swiper.realIndex, 0);
            }
        });

        $popupSliderModal.on('shown.bs.modal', function(e) {
            popupSwiper.update();
        });

        allSwipers.push(swiper, popupSwiper);

    });

    $(".b-gallery__slide").on('click', function(event) {
        if ($(window).width() < 768) {
            return false;
        }
    });

    $(".l-gallery:first").parent()
        .on('transitStart', (event) => {
            $(".b-popup-gallery.in").removeClass('fade').modal("hide");
        })
        .on('transitEnd', (event) => {

            for (var i = allSwipers.length - 1; i >= 0; i--) {
                allSwipers[i].destroy(true, false);
            }

            allSwipers = null;
        });

})();
