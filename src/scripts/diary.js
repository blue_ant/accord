(function() {
    let $cont = $(".l-diary:first");

    function stopPlayingVid(cont) {
        let $cont;
        if (cont) {
            $cont = $(cont);
        }
        $cont.find("iframe").each(function(index, el) {
            setTimeout(() => {
                let player = new Vimeo.Player(el);
                player.pause();
            }, 0);
        });
    }

    let $popupSliderModal = $("#diaryPopupGalleryModal");
    $popupSliderModal
        .on("show.bs.modal", function(event) {
            popupSwiper.onResize();
        })
        .on("shown.bs.modal", function(event) {
            popupSwiper.update();
            if (popupSwiper.realIndex === 0) {
                let iframe = popupSwiper.container[0].getElementsByTagName("iframe")[0];
                let player = new Vimeo.Player(iframe);
                player.play();
            }
        })
        .on("hide.bs.modal", function(event) {
            stopPlayingVid(event.currentTarget);
        })
        .on("hidden.bs.modal", () => {
            popupSwiper.removeAllSlides();
        });

    let allSwipers = [];

    let popupSwiper = new Swiper($(".b-popupgallery__slider"), {
        slidesPerView: 1,
        centeredSlides: true,
        touchEventsTarget: "slide",
        lazyLoading: true,
        lazyLoadingInPrevNext: true,
        lazyLoadingInPrevNextAmount: 1,
        nextButton: $popupSliderModal.find(".b-popup-gallery__navbtn--next"),
        prevButton: $popupSliderModal.find(".b-popup-gallery__navbtn--prev"),
        loop: false,
        onTransitionEnd: (swiper) => {
            if (swiper.previousIndex === 0) {
                stopPlayingVid(swiper.container);
            }
        },
    });

    allSwipers.push(popupSwiper);

    $(".b-diary-slider").each(function(index, el) {
        let $block = $(el);

        let swiper = new Swiper($block.find(".swiper-container:first"), {
            pagination: $block.find(".b-diary-slider__pager:first"),
            slidesPerView: "auto",
            centeredSlides: true,
            paginationClickable: true,
            paginationType: "fraction",
            touchEventsTarget: "wrapper",
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            speed: 300,
            nextButton: $block.find(".b-diary-slider__navbtn--next:first"),
            prevButton: $block.find(".b-diary-slider__navbtn--prev:first"),
            paginationFractionRender: function(swiper, currentClassName, totalClassName) {
                return (
                    '<div class="b-diary-slider__counter b-diary-slider__counter--current ' +
                    currentClassName +
                    '"></div><div class="b-diary-slider__counter b-diary-slider__counter--total ' +
                    totalClassName +
                    '"></div>'
                );
            },
        });

        allSwipers.push(swiper);
    });

    var primeSlider = new Swiper(".b-diary-primeslider", {
        centeredSlides: false,
        slidesPerView: "auto",
        touchEventsTarget: "wrapper",
        direction: "vertical",
        mousewheelControl: true,
        onlyExternal: true,

        onSlideChangeStart: function(swiper) {
            var $months = $(".b-diary-months-slider__item");
            $months.filter(".b-diary-months-slider__item--prev:first").removeClass("b-diary-months-slider__item--prev");

            var $needed = $months.eq(swiper.realIndex);
            $needed.addClass("b-diary-months-slider__item--prev");

            var slideTitle = $needed.next().html();
            $(".b-diary-slider__mobtitle:first").html(slideTitle);
        },
        nextButton: $cont.find(".b-diary-slider__mobswitcher-navbtn--next:first"),
        prevButton: $cont.find(".b-diary-slider__mobswitcher-navbtn--prev:first"),
        onTransitionEnd: (swiper) => {
            primeSlider.lastUserSettedIndex = primeSlider.realIndex;
            stopPlayingVid(swiper.slides[swiper.previousIndex]);
        },
    });

    primeSlider.lastUserSettedIndex = 0;

    allSwipers.push(primeSlider);

    let $window = $(window);

    $window.on(
        "resize.diary",
        _.debounce(() => {
            for (var i = 0; i < allSwipers.length - 1; i++) {
                allSwipers[i].update();
                allSwipers[i].slideTo(allSwipers[i].realIndex, 0);
            }

            if (primeSlider.lastUserSettedIndex != primeSlider.realIndex) {
                primeSlider.update();
                primeSlider.slideTo(primeSlider.lastUserSettedIndex, 0);
            }
        }, 100)
    );

    $(".b-diary-months-slider").on("click", ".b-diary-months-slider__item", function(event) {
        var $monthTitle = $(event.currentTarget);
        var $p = $monthTitle.prev().prev();
        var clsName = "b-diary-months-slider__item--prev";
        if ($p.hasClass(clsName)) {
            primeSlider.slideNext();
        } else {
            var $pp = $p.prev();
            if ($pp.hasClass(clsName)) {
                primeSlider.slideNext();
                setTimeout(primeSlider.slideNext, 0);
            }
        }
    });

    $cont.find(".b-diary-slider__slide").on("click", (event) => {
        event.preventDefault();
        if ($window.width() < 768) return false;

        showPagePreloader();

        let $clickedSlide = $(event.currentTarget);

        $.ajax({
            url: $clickedSlide.attr("href"),
            dataType: "json",
        })
            .done((data) => {
                let slides = [];
                data.forEach((sld) => {
                    let sldMarkup;
                    if (sld.type === "iframe") {
                        sldMarkup = `
<div class="swiper-slide">
    <div class="b-popup-gallery__slide-inner">
        <div class="b-popup-gallery__popup-video-wrap va">
            <div class="va-item">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="${sld.src}"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
`;
                    } else {
                        sldMarkup = `
<div class="swiper-slide">
    <div class="b-popup-gallery__slide-inner"> <img class="b-popup-gallery__img swiper-lazy" src="${sld.src}" alt="">
        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
    </div>
</div>
`;
                    }

                    slides.push(sldMarkup);
                });

                popupSwiper.appendSlide(slides);
                
                popupSwiper.setWrapperTranslate($clickedSlide.index() * $window.width() * -1);
                popupSwiper.slideTo($clickedSlide.index());
                $popupSliderModal.modal("show");
            })
            .fail(function() {
                alert(
                    "Не удалось получить список слайдов! Пожалуйста поробуйте позже или обратитесь к администрации сайта."
                );
            })
            .always(function() {
                hidePagePreloader();
            });
    });



    {
        let $webcamModal = $("#webcamStreamModal");
        let $webcamStreamVideoContainer = $webcamModal.find("#webcamStreamVideoContainer");
        $webcamModal.on('hidden.bs.modal',  (event)=> {
            event.preventDefault();
            $webcamStreamVideoContainer.empty();
        });
        
        $cont
            .find("#webcams")
            .find(".b-webcams__item")
            .on("click", (event) => {
                event.preventDefault();
                console.log($(("#webcamStreamModal")));
                $webcamStreamVideoContainer.html(`<iframe class="embed-responsive-item" src="${event.currentTarget.href}"></iframe>`);
                $webcamModal.modal("show");
            });
    }

    $cont
        .parent()
        .on("transitStart", (event) => {
            $window.off(".diary");
            $(".b-popup-gallery.in")
                .removeClass("fade")
                .modal("hide");
        })
        .on("transitEnd", (event) => {
            for (var i = allSwipers.length - 1; i >= 0; i--) {
                allSwipers[i].destroy(true, false);
            }

            allSwipers = null;
        });

})();
