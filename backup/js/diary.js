"use strict";

(function () {
    var $cont = $(".l-diary:first");
    var allSwipers = [];
    var $popupSliders = $(".b-popupgallery__slider");

    function stopPlayingVid(cont) {
        var $cont = void 0;
        if (cont) {
            $cont = $(cont);
        }
        $cont.find("iframe").each(function (index, el) {
            setTimeout(function () {
                var player = new Vimeo.Player(el);
                player.pause();
            }, 0);
        });
    }

    $(".b-diary-slider").each(function (index, el) {
        var $block = $(el);
        var $popupSliderModal = $block.next();

        var popupSwiper = new Swiper($popupSliders.eq(index), {
            slidesPerView: 1,
            centeredSlides: true,
            touchEventsTarget: 'slide',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 1,
            nextButton: $popupSliderModal.find('.b-popup-gallery__navbtn--next'),
            prevButton: $popupSliderModal.find('.b-popup-gallery__navbtn--prev'),
            loop: false,
            onTransitionEnd: function onTransitionEnd(swiper) {

                if (swiper.previousIndex === 0) {
                    stopPlayingVid(swiper.container);
                }
            }
        });

        var swiper = new Swiper($block.find(".swiper-container:first"), {
            pagination: $block.find(".b-diary-slider__pager:first"),
            slidesPerView: 'auto',
            centeredSlides: true,
            paginationClickable: true,
            paginationType: 'fraction',
            touchEventsTarget: 'wrapper',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            speed: 300,
            nextButton: $block.find(".b-diary-slider__navbtn--next:first"),
            prevButton: $block.find(".b-diary-slider__navbtn--prev:first"),
            paginationFractionRender: function paginationFractionRender(swiper, currentClassName, totalClassName) {
                return '<div class="b-diary-slider__counter b-diary-slider__counter--current ' + currentClassName + '"></div><div class="b-diary-slider__counter b-diary-slider__counter--total ' + totalClassName + '"></div>';
            },

            onTransitionEnd: function onTransitionEnd(swiper) {
                popupSwiper.slideTo(swiper.realIndex, 0);
                if (swiper.previousIndex === 0) {
                    stopPlayingVid(swiper.container);
                }
            }
        });

        allSwipers.push(swiper, popupSwiper);

        $popupSliderModal.on('shown.bs.modal', function (event) {
            popupSwiper.update();
            if (popupSwiper.realIndex === 0) {
                var iframe = popupSwiper.container[0].getElementsByTagName('iframe')[0];
                var player = new Vimeo.Player(iframe);
                player.play();
            }
        }).on('hide.bs.modal', function (event) {
            stopPlayingVid(event.currentTarget);
        });

        // if (index === 1) {
        //     return false;
        // }
    });

    $(".b-popup-gallery").appendTo(".l-diary:first");

    var primeSlider = new Swiper(".b-diary-primeslider", {
        centeredSlides: false,
        slidesPerView: 'auto',
        touchEventsTarget: 'wrapper',
        direction: "vertical",
        mousewheelControl: true,
        onlyExternal: true,

        onSlideChangeStart: function onSlideChangeStart(swiper) {

            var $months = $(".b-diary-months-slider__item");
            $months.filter(".b-diary-months-slider__item--prev:first").removeClass('b-diary-months-slider__item--prev');

            var $needed = $months.eq(swiper.realIndex);
            $needed.addClass('b-diary-months-slider__item--prev');

            var slideTitle = $needed.next().html();
            $(".b-diary-slider__mobtitle:first").html(slideTitle);
        },
        nextButton: $cont.find('.b-diary-slider__mobswitcher-navbtn--next:first'),
        prevButton: $cont.find('.b-diary-slider__mobswitcher-navbtn--prev:first'),
        onTransitionEnd: function onTransitionEnd(swiper) {
            primeSlider.lastUserSettedIndex = primeSlider.realIndex;
            stopPlayingVid(swiper.slides[swiper.previousIndex]);
        }

    });

    primeSlider.lastUserSettedIndex = 0;

    allSwipers.push(primeSlider);
    $(window).on('resize.diary orientationchange.diary', _.debounce(function () {

        for (var i = 0; i < allSwipers.length - 1; i++) {
            allSwipers[i].update();
            allSwipers[i].slideTo(allSwipers[i].realIndex, 0);
        }

        if (primeSlider.lastUserSettedIndex != primeSlider.realIndex) {
            primeSlider.update();
            primeSlider.slideTo(primeSlider.lastUserSettedIndex, 0);
        }
    }, 100));

    $(".b-diary-months-slider").on('click', '.b-diary-months-slider__item', function (event) {
        var $monthTitle = $(event.currentTarget);
        var $p = $monthTitle.prev().prev();
        var clsName = 'b-diary-months-slider__item--prev';
        if ($p.hasClass(clsName)) {
            primeSlider.slideNext();
        } else {
            var $pp = $p.prev();
            if ($pp.hasClass(clsName)) {
                primeSlider.slideNext();
                setTimeout(primeSlider.slideNext, 0);
            }
        }
    });

    $(".b-diary-slider__slide").on('click', function (event) {

        if ($(window).width() < 768) {
            event.preventDefault();
            return false;
        }
    });

    $(".l-diary:first").parent().on('transitStart', function (event) {
        $(window).off('.diary');
        $(".b-popup-gallery.in").removeClass('fade').modal("hide");
    }).on('transitEnd', function (event) {

        for (var i = allSwipers.length - 1; i >= 0; i--) {
            allSwipers[i].destroy(true, false);
        }

        allSwipers = null;
    });
})();