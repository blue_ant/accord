'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Handlebars.registerHelper('if_eq', function (a, b, opts) {
    if (a == b) // Or === depending on your needs
        return opts.fn(this);else return opts.inverse(this);
});

moment.locale('ru');

function getQuartersFromDateRange(start, end) {
    var quarterPoints = [moment(start).unix()];
    var tmpQuarterPoint = new Date(start);

    while (tmpQuarterPoint < end) {
        tmpQuarterPoint.setMonth(tmpQuarterPoint.getMonth() + 3);
        quarterPoints.push(moment(tmpQuarterPoint).unix());
    }

    return quarterPoints;
}

var favoritPlansModalTpl = Handlebars.compile('\n<table class="b-starred-flats-modal__miniplans table">\n    <tbody>\n        <tr> \n        {{#each flats}}\n            <td>\n                <div class="b-starred-flats-modal__miniplan">\n                    <div class="b-starred-flats-modal__plan-hint">\u041F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0430</div>\n                    <div class="b-starred-flats-modal__plan-name">{{name}}</div> \n                    <div class="b-starred-flats-modal__img-wrap"><img src="{{img}}" alt="" class="b-starred-flats-modal__img">\n                    <a href="{{url}}" class="b-starred-flats-modal__img-link b-starred-flats-modal__img-link--go"></a>\n                    <a href="#" class="b-starred-flats-modal__img-link b-starred-flats-modal__img-link--remove" data-flat-id="{{id}}"></a>\n                    </div>     \n                </div>\n            </td>\n        {{/each}}\n        </tr>\n    </tbody>\n</table>\n<div class="clearfix">\n    <table class="b-starred-flats-modal__table b-starred-flats-modal__table--values table">\n        <tbody>\n            <tr> \n                {{#each flats}}\n                <td>\n                    <div class="b-starred-flats-modal__plan-val">\n                        {{#if_eq area_min area_max}}\n                          {{area_min}} m<sup>2</sup>\n                        {{else}}\n                          {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>\n                        {{/if_eq}}\n                    </div>\n                </td>\n                {{/each}}\n            </tr>\n            <tr>\n            {{#each flats}}\n                <td>\n                    <div class="b-starred-flats-modal__plan-val">{{bedrooms}}</div>\n                </td>\n            {{/each}}\n            </tr>\n            <tr>\n            {{#each flats}}\n                <td>\n                    <div class="b-starred-flats-modal__plan-val">+</div>\n                </td>\n            {{/each}}\n            </tr>\n            <tr> \n            {{#each flats}}\n                <td>\n                    <div class="b-starred-flats-modal__plan-val">+</div>\n                </td>\n            {{/each}}\n            </tr>\n            <tr>\n            {{#each flats}}\n                <td>\n                    <div class="b-starred-flats-modal__plan-val">+</div>\n                </td>\n            {{/each}}\n            </tr>\n            <tr>\n                {{#each flats}}\n                <td>+</td>\n                {{/each}}\n            </tr>\n        </tbody>\n    </table>\n</div>\n');

function insertFavoritPlansPopup() {
    var isNew = false;
    if (!document.getElementById('starredFlatsModal')) {
        isNew = true;
        $("body:first").after('\n<div class="modal modal-centered fade hidden-xs" id="starredFlatsModal">\n    <div class="modal-dialog modal-fluid">\n        <div class="modal-content">\n            <div class="modal-body" style="position: relative;">\n                <div class="b-starred-flats-modal b-starred-flats-modal--empty clearfix">\n                    <div class="b-starred-flats-modal__left">\n                        <div class="b-starred-flats-modal__title">\u0418\u0437\u0431\u0440\u0430\u043D\u043D\u043E\u0435</div>\n                        <table class="b-starred-flats-modal__table b-starred-flats-modal__table--headings table">\n                            <tbody>\n                                <tr>\n                                    <td>\u041F\u043B\u043E\u0449\u0430\u0434\u044C</td>\n                                </tr>\n                                <tr>\n                                    <td>\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u0441\u043F\u0430\u043B\u0435\u043D</td>\n                                </tr>\n                                <tr>\n                                    <td>\u0411\u0430\u043B\u043A\u043E\u043D</td>\n                                </tr>\n                                <tr>\n                                    <td>\u0421\u0438\u0441\u0442\u0435\u043C\u0430 "\u0423\u043C\u043D\u044B\u0439 \u0414\u043E\u043C"</td>\n                                </tr>\n                                <tr>\n                                    <td>Smart-\u043F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0430</td>\n                                </tr>\n                                <tr>\n                                    <td>\u041F\u0430\u043D\u043E\u0440\u0430\u043C\u043D\u044B\u0435 \u043E\u043A\u043D\u0430</td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                    <div class="b-starred-flats-modal__right">\n                        <div class="b-starred-flats-modal__right-cont"></div>\n                    </div>\n                    <div class="b-starred-flats-modal__emptytext">\n                        \u041D\u0435\u0442 \u043A\u0432\u0430\u0440\u0442\u0438\u0440 <br>\u0432 &laquo;\u0438\u0437\u0431\u0440\u0430\u043D\u043D\u043E\u043C&raquo;\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n');
    }

    var $modal = $('#starredFlatsModal');
    var $miniplansWrap = $modal.find('.b-starred-flats-modal__right:first');

    if (isNew) {
        $miniplansWrap.perfectScrollbar({
            suppressScrollY: true,
            useBothWheelAxes: true,
            useKeyboard: false
        });

        $miniplansWrap.on('click', ".b-starred-flats-modal__img-link--remove", function (event) {
            event.preventDefault();
            showPagePreloader();
            var $inner = $modal.find('.b-starred-flats-modal:first');

            var $miniplan = $(event.currentTarget).parents("td:first");
            var $table = $(".b-starred-flats-modal__table--values:first");
            var colNum = $miniplan.index() + 1;
            var $cols = $table.find('td:nth-child(' + colNum + ')');
            var $elsToDelete = $miniplan.add($cols);
            TweenLite.to($elsToDelete, 0.4, {
                alpha: 0,
                scale: 0.9,
                ease: Power1.easeOut,
                onComplete: function onComplete() {
                    var planCount = $table.find('tr:first td').length;
                    if (planCount === 1) {
                        $inner.addClass('b-starred-flats-modal--empty');
                    } else {
                        $elsToDelete.remove();
                        $miniplansWrap.perfectScrollbar("update");
                    }

                    var flatId = parseInt(event.currentTarget.dataset.flatId);

                    toggleFavoriteFlat(flatId);
                }
            });
        }).on('click', '.b-starred-flats-modal__img-link', function (event) {
            if (document.getElementsByClassName('l-single-flat').length) {
                event.preventDefault();
                showPagePreloader();
                window.location.href = event.currentTarget.href;
            }
        });
    }

    $modal.on('shown.bs.modal', function (event) {
        $miniplansWrap.perfectScrollbar("update");
    });

    $(window).on('resize orientationchange', function (event) {
        if ($modal.data('bs.modal') && $modal.data('bs.modal').isShown) {
            if ($(window).width() < 768) {
                $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
            } else {
                $miniplansWrap.perfectScrollbar("update");
            }
        }
    });

    return {
        miniplansWrap: $miniplansWrap,
        modal: $modal
    };
}

function refreshFavoritPlansList(cb) {

    $.ajax({
        url: '/apartments/?ajax=1&get_typicals=1&get_favorites=1'
    }).done(function (data) {
        var $modal = $('#starredFlatsModal');
        var $inner = $modal.find('.b-starred-flats-modal:first');
        var $stars = $(".b-single-flat__title-star[data-flat-id]:first, .b-flatcard__iconbtn[data-flat-id]");
        var ids = [];

        for (var i = data.typicals.length - 1; i >= 0; i--) {
            ids.push(data.typicals[i].id + "");
        }

        if (data.typicals.length) {
            $stars.removeClass('b-single-flat__title-star--active b-flatcard__iconbtn--active').filter(function (index, el) {
                return _.includes(ids, el.dataset.flatId);
            }).each(function (index, el) {
                var className = el.classList.item(0);
                el.classList.add(className + "--active");
            });

            var markup = favoritPlansModalTpl({ flats: data.typicals });
            $modal.find('.b-starred-flats-modal__right-cont:first').html(markup);
            $inner.removeClass('b-starred-flats-modal--empty');
        } else {
            $inner.addClass('b-starred-flats-modal--empty');
            $stars.removeClass('b-single-flat__title-star--active b-flatcard__iconbtn--active');
        }
        if (cb) {
            cb();
        }
    });
}

function toggleFavoriteFlat(id, cb) {
    showPagePreloader();
    $.ajax({
        url: 'http://acc.blue-ant.ru/apartments/?favorite=' + id
    }).done(function (data) {
        refreshFavoritPlansList();
        hidePagePreloader();
        if (cb) {
            cb(data);
        }
    });
}

function genplanShow() {
    var genplan = document.getElementById('genplan');
    var track = genplan.getElementsByClassName('b-genplan__track')[0];
    pullGenplanImg();
    TweenLite.to(genplan, 0.4, {
        autoAlpha: 1,
        onComplete: function onComplete() {

            TweenLite.to(track, 0.4, { left: "0%", ease: Power1.easeInOut });
        }
    });
}

function genplanHide() {
    var genplan = document.getElementById('genplan');
    var track = genplan.getElementsByClassName('b-genplan__track')[0];
    TweenLite.to(track, 0.4, {
        left: "100%",
        ease: Power1.easeInOut,
        onComplete: function onComplete() {
            TweenLite.to(genplan, 0.4, { autoAlpha: 0 });
        }
    });
}

function insertGenplanModal() {
    if (!document.getElementById('genplan')) {
        $("body:first").after('\n<div class="b-genplan" id="genplan">\n    <div class="b-genplan__inner">\n    <div class="b-genplan__track">\n        <div class="b-genplan__img-wrap">\n            <img src="/img/genplan/genplan.jpg" alt="" class="b-genplan__img">\n            <a href="#" class="b-genplan__overlink b-genplan__overlink--1A" data-house-num-title="1A" data-house="11">\u041A\u043E\u0440\u043F\u0443\u0441 <b>1A</b></a>\n            <div class="b-genplan__overpic b-genplan__overpic--1A"></div>\n            <a href="#" class="b-genplan__overlink b-genplan__overlink--2A" data-house-num-title="2A" data-house="13">\u041A\u043E\u0440\u043F\u0443\u0441 <b>2A</b></a>\n            <div class="b-genplan__overpic b-genplan__overpic--2A"></div>\n            <a href="#" class="b-genplan__overlink b-genplan__overlink--1B" data-house-num-title="1B" data-house="12">\u041A\u043E\u0440\u043F\u0443\u0441 <b>1B</b></a>\n            <div class="b-genplan__overpic b-genplan__overpic--1B"></div>\n            <a href="#" class="b-genplan__overlink b-genplan__overlink--2B" data-house-num-title="2B" data-house="14">\u041A\u043E\u0440\u043F\u0443\u0441 <b>2B</b></a>\n            <div class="b-genplan__overpic b-genplan__overpic--2B"></div>\n        </div>\n        <div class="b-windrose b-windrose--genplan hidden-xs"></div>\n        <div class="b-genplan__show-btn btn btn-sm btn-pale" onclick="genplanHide();"></div>\n    </div>\n    </div>\n    <div class="b-genplan__close-btn" onclick="genplanHide();"></div>\n    \n\n</div>\n');
        $(document).on('click', '[data-house-num-title]', function (event) {

            event.preventDefault();
            var link = event.currentTarget;
            var valId = link.dataset.house;
            var house = link.dataset.houseNumTitle;

            var $housesInputs = $("[name='house\[\]']");

            link.classList.toggle("b-genplan__overlink--active");

            var isChecked = link.classList.contains('b-genplan__overlink--active');

            $('[name=\'house[]\'][value=\'' + valId + '\']:first').attr('checked', isChecked).trigger('change');
            populateGenplanHousesBtns();
        });

        $(window).on('resize orientationchange', _.debounce(function () {

            pullGenplanImg();
        }, 100));
    }
}

function pullGenplanImg() {
    var $window = $(window);

    var $genplanBlock = $("#genplan");
    var $wrap = $genplanBlock.find('.b-genplan__img-wrap:first');

    if ($window.width() < 768) {
        TweenLite.set($wrap, {
            clearProps: "all"
        });

        return false;
    }

    var $track = $genplanBlock.find('.b-genplan__inner:first');
    var $img = $genplanBlock.find('.b-genplan__img:first');

    var wrapWidth = $track.width();
    var wrapHeight = $window.height();

    var imgWidth = $img.width();
    var imgHeight = $img.height();

    if (wrapWidth / imgWidth > wrapHeight / imgHeight) {
        TweenLite.set($img, {
            width: wrapWidth,
            height: "auto"
        });
    } else {
        TweenLite.set($img, {
            height: wrapHeight,
            width: "auto"
        });
    }

    var newImgHeight = $img.height();
    var newImgWidth = $img.width();

    TweenLite.set($wrap, {
        marginLeft: newImgWidth / -2,
        marginTop: newImgHeight / -2
    });

    return true;
}

function populateGenplanHousesBtns() {
    var $checkedLinks = $('.b-genplan__overlink--active');
    var $btn = $('.b-flats-filter__round-btn--build');
    var $showResultBtn = $('.b-genplan__show-btn:first');

    if ($checkedLinks.length) {

        var btnStr = "";
        $checkedLinks.each(function (index, el) {
            var prefix = "";
            if (index !== 0) {
                prefix = ", ";
            }
            btnStr += prefix + el.dataset.houseNumTitle;
        });

        $btn.addClass('b-flats-filter__round-btn--wo-icon').text(btnStr);

        $showResultBtn.addClass('b-genplan__show-btn--active');
    } else {
        $btn.removeClass('b-flats-filter__round-btn--wo-icon').text("");

        $showResultBtn.removeClass('b-genplan__show-btn--active');
    }
}

function preserveFiltersParams(opts) {
    var $inputs = opts.inputs;
    if (opts.skip) {
        $inputs = $inputs.filter(function (index, element) {
            return _.indexOf(opts.skip, element.name) === -1;
        });
    }
    var inputsArr = $inputs.serializeArray();

    localStorage.setItem(opts.storageID, JSON.stringify(inputsArr));
}

function restoreFiltersParams(opts) {
    var $form = opts.form;
    var storageID = opts.storageID;

    var preservedFiltersData = localStorage.getItem(storageID);
    if (!preservedFiltersData) return;

    var statesArr = JSON.parse(preservedFiltersData);
    statesArr = _.groupBy(statesArr, "name");

    var areaSliderData = [0, 0];
    var realeseSliderData = [];

    for (var k in statesArr) {

        if (opts.skip && _.indexOf(opts.skip, k) !== -1) {
            continue;
        }
        var $inputsByName = $form.find('[name="' + k + '"]');
        for (var i = statesArr[k].length - 1; i >= 0; i--) {
            var storedVal = statesArr[k][i].value;

            if ($inputsByName.attr('type') === "checkbox") {
                var $inp = $inputsByName.filter('[value=\'' + storedVal + '\']:first');
                $inp.attr('checked', true);
            } else {
                var name = $inputsByName.attr('name');

                if (name === "area_from") {
                    areaSliderData[0] = parseInt(storedVal);
                } else if (name === "area_to") {
                    areaSliderData[1] = parseInt(storedVal);
                } else if (name === "finish_from") {
                    realeseSliderData[0] = parseInt(storedVal);
                } else if (name === "finish_to") {
                    realeseSliderData[1] = parseInt(storedVal);
                }

                $inputsByName.val(storedVal);
            }
        }
    }

    if (areaSliderData[0]) {
        $form.find('[data-area-slider]:first').slider("setValue", areaSliderData, true);
    }

    if (realeseSliderData[0]) {
        $form.find('[data-realese-slider]:first').slider("setValue", realeseSliderData, true);
    }

    var $genplanLinks = $(".b-genplan__overlink");

    $form.find("[name='house\[\]']").each(function (index, el) {
        if (el.checked) {
            $genplanLinks.filter('[data-house="' + el.value + '"]').addClass('b-genplan__overlink--active');
        }
    });

    populateGenplanHousesBtns();
}

function showMainMenu() {
    var $window = $(window);
    var $menu = $("#mainMenu");
    var $ball = $menu.find('.b-menu__ball:first');
    var $inner = $menu.find('.b-menu__inner:first');
    var tl = new TimelineLite();
    var $closeBtn = $menu.find('.b-menu__close-btn:first');
    var h = $window.height();
    var w = $window.width();

    var hypot = Math.sqrt(w * w + h * h) * 2;

    tl.set($menu, { visibility: "visible", background: "none", pointerEvents: "none" }).add('animeStart').to($ball, 0.6, {
        width: hypot
    }).from($closeBtn, 0.3, {
        rotation: 360,
        opacity: 0
    }, "animeStart").to($inner, 0.2, {
        opacity: 1
    }).set($menu, { clearProps: "background,pointerEvents" });
}

function hideMainMenu() {

    var $menu = $("#mainMenu");
    var $ball = $menu.find('.b-menu__ball:first');
    var $inner = $menu.find('.b-menu__inner:first');
    var $closeBtn = $menu.find('.b-menu__close-btn:first');

    var tl = new TimelineLite();
    tl.set($menu, { background: "none", pointerEvents: "none" }).add("animeStart").to($inner, 0.2, {
        opacity: 0
    }, "animeStart").to($closeBtn, 0.3, {
        rotation: 360,
        opacity: 0
    }, "animeStart").to($ball, 0.6, {
        width: 0
    }).set([$menu, $ball, $inner, $closeBtn], { clearProps: "all" });
}

function setActiveMainMenuLink() {
    var url = window.location.href;
    var $links = $('.b-menu__leaf');
    $links.removeClass('b-menu__leaf--active').each(function (index, el) {

        if (_.includes(url, el.href)) {
            el.classList.add("b-menu__leaf--active");
            return false;
        }
    });
}

function showPagePreloader(cb) {
    var preloaderOverlay = document.getElementById("pagePreloader");
    if (preloaderOverlay) {
        var pause = 0.2;
        /*if (window.accordAppStates.isCloudsAnimeDisabled) {
            pause = 1.4;
        }*/
        TweenLite.killTweensOf(preloaderOverlay);
        TweenLite.to(preloaderOverlay, 0.2, {
            autoAlpha: 1,
            delay: pause,
            onComplete: function onComplete() {
                if (cb) {
                    cb();
                }
            }
        });
    }
}

function hidePagePreloader(cb) {

    if (document.readyState !== "complete" || window.accordAppStates.isPreloaderLockedOn) {
        return false;
    }

    var preloaderOverlay = document.getElementById("pagePreloader");
    if (preloaderOverlay) {
        TweenLite.killTweensOf(preloaderOverlay);
        TweenLite.to(preloaderOverlay, 0.2, {
            autoAlpha: 0,
            onComplete: function onComplete() {
                if (cb) {
                    cb();
                }
            }
        });
    }
}

function clearFilterForm(id) {
    var $form = $(id);
    $form.find('input[type="checkbox"]').prop('checked', false);
    //localStorage.removeItem("flatsFiltersFormStorage");
    $form.find('.b-multycheckbox__input').each(function (index, el) {
        var $inp = $(el);
        var possibleValues = $inp.data('possible-values');
        $inp.val(possibleValues[0]);
    });

    $form.find('[data-slider-value]').each(function (index, el) {
        var $rangeSlider = $(el);
        var initialValues = $rangeSlider.data("slider-value");
        $rangeSlider.slider("setValue", initialValues, true);
    });

    var $activeHouseLinks = $("#genplan").find(".b-genplan__overlink--active");

    if ($activeHouseLinks.length) {
        $activeHouseLinks.trigger('click');
    } else {
        $form.find('input:first').trigger('change');
    }
}

function switchScreen(opts, cb) {
    $(".b-aside-form__backbtn").trigger('click');
    hideMainMenu();
    var $preloaderOverlay = $("#pagePreloader");
    showPagePreloader();
    $.ajax({
        url: opts.path,
        dataType: 'html'
    }).always(function (data) {
        var $cont = $("#pageContainer");
        var $oldView = $cont.find('.l-main__inner');
        var $newDoc = void 0;
        if (data.responseText) {
            $newDoc = $(data.responseText);
        } else {
            $newDoc = $(data);
        }

        var $newView = $newDoc.find(".l-main__inner:first");

        var pageTypesMap = {
            "l-404": {
                isTransitWithClouds: false
            },
            "b-homepage-slides": {
                isTransitWithClouds: false
            },
            "l-about": {
                isTransitWithClouds: true
            },

            "l-action": {
                isTransitWithClouds: false
            },
            "l-actions": {
                isTransitWithClouds: false
            },
            "l-article": {
                isTransitWithClouds: false
            },
            "l-buy": {
                isTransitWithClouds: true
            },
            "l-contacts": {
                isTransitWithClouds: true
            },
            "l-diary": {
                isTransitWithClouds: false
            },
            "l-docs": {
                isTransitWithClouds: false
            },
            "l-single-flat": {
                isTransitWithClouds: false,
                preserveFilters: true
            },
            "l-flats": {
                isTransitWithClouds: false,
                preserveFilters: true
            },

            "l-storerooms": {
                isTransitWithClouds: false,
                preserveFilters: true
            },

            "l-furnish": {
                isTransitWithClouds: false
            },
            "l-gallery": {
                isTransitWithClouds: false
            },
            "l-hypothec": {
                isTransitWithClouds: false
            },
            "l-infras": {
                isTransitWithClouds: true
            },
            "l-news": {
                isTransitWithClouds: false
            },
            "l-baby": {
                isTransitWithClouds: false
            },
            "l-tour-entry": {
                isTransitWithClouds: false
            }
        };

        var wrapperClassName = $newView.find(' > div').not('.hostcmsPanel').get(0).classList.item(0);
        var oldWrapperClassName = $oldView.find(' > div').not('.hostcmsPanel').get(0).classList.item(0);

        var oldPageOpts = pageTypesMap[oldWrapperClassName];
        var pageOpts = pageTypesMap[wrapperClassName];
        if (!pageOpts.preserveFilters) {
            localStorage.removeItem("flatsFiltersFormStorage");
        }
        document.title = $newDoc.filter('title:first').text();

        TweenLite.set($newView, { alpha: 0 });
        $cont.append($newView);
        $oldView.trigger('transitStart');

        //TweenMax.killAll(false, false, true, false);

        var tl = new TimelineLite();
        if ((pageOpts.isTransitWithClouds || oldPageOpts.isTransitWithClouds) && !window.accordAppStates.isCloudsAnimeDisabled && $(window).width() >= 758) {
            hidePagePreloader();
            var $cloudsBg = $("#cloudsOverlay");
            TweenMax.killChildTweensOf($cloudsBg);
            tl.set($cloudsBg, { display: "block" }).add("crossfade").to($cloudsBg, 1.2, {
                y: "-100%"
            }, "crossfade").to($oldView, 1, {
                alpha: 0,
                delay: 1

            }, "crossfade").add("cloudFadeout").to($cloudsBg, 1.2, {
                opacity: 0.7,
                y: "0%",
                ease: Power1.easeIn
            }, "cloudFadeout").to($newView, 0.6, {
                css: { alpha: 1 }

            }, "cloudFadeout").set($cloudsBg, {
                clearProps: "all",
                onComplete: function onComplete() {
                    window.scrollTo(0, 0);
                    $oldView.trigger('transitEnd').remove();
                    setActiveMainMenuLink();

                    if (cb) cb();
                }
            });
        } else {
            hidePagePreloader();
            tl.add("crossfade").to($newView, 1, {
                css: { alpha: 1 }

            }, "crossfade").to($oldView, 1, {
                alpha: 0,
                onComplete: function onComplete() {
                    $oldView.trigger('transitEnd').remove();
                    setActiveMainMenuLink();

                    if (cb) cb();
                }
            }, "crossfade");
        }
        window.accordAppStates.isCloudsAnimeDisabled = false;
    });
}

window.accordAppStates = {
    isCloudsAnimeDisabled: false
};

$(document).on('click', ".b-menu__leaf", function (event) {
    window.accordAppStates.isCloudsAnimeDisabled = true;
}).on('click', '.b-multycheckbox__btn', function (event) {
    event.preventDefault();
    var btn = event.currentTarget;
    var $inp = $(btn.parentNode.getElementsByTagName('input')[0]);
    var possibleValues = $inp.data("possible-values");
    for (var i = possibleValues.length - 1; i >= 0; i--) {
        possibleValues[i] = possibleValues[i] + "";
    }
    var currentVal = $inp.val() + "";
    var currentValIndex = _.indexOf(possibleValues, currentVal);
    var newVal = void 0;
    if (currentValIndex === possibleValues.length - 1) {
        newVal = possibleValues[0];
    } else {
        newVal = possibleValues[currentValIndex + 1];
    }
    $inp.val(newVal).trigger('change');
}).ready(function () {
    $("#pagePreloader").on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });
}).on('click', '[data-aside-menu]', function (event) {
    event.preventDefault();
    $(".b-aside-form__backbtn").trigger('click');
    var targetMenu = document.getElementById(event.currentTarget.dataset.asideMenu);
    var targetXposition = "100%";
    if (targetMenu.classList.contains("b-aside-form--right")) {
        targetXposition = "-100%";
    }
    TweenLite.to(targetMenu, 0.45, { x: targetXposition, ease: Power1.easeInOut });
}).on('click', '[data-dismiss-aside-menu]', function (event) {
    event.preventDefault();
    var targetMenu = void 0;
    var link = event.currentTarget;

    if (link.dataset.dismissAsideMenu) {
        targetMenu = document.getElementById(event.currentTarget.dataset.asideMenu);
    } else {
        targetMenu = link.parentNode;
    }
    TweenLite.to(targetMenu, 0.45, {
        x: '0%',
        ease: Power1.easeInOut,
        onComplete: function onComplete() {
            var $form = $(targetMenu).find("form:first");
            var validator = $form.trigger('reset').data("validator");
            if (validator) {
                validator.resetForm();
            }

            TweenLite.set([$form, targetMenu.getElementsByClassName('b-aside-form__header')], { clearProps: "all" });
            $form.next(".b-aside-form__success-text").remove();
        }
    });
}).on('click', '.b-nav__burger', function (event) {
    showMainMenu();
}).on('click', '.b-flats__controls-btn', function (event) {
    event.preventDefault();
    event.currentTarget.nextElementSibling.classList.add("l-flats__filters--active");
}).on('click', '.b-flats-filter__close', function (event) {
    event.preventDefault();
    event.currentTarget.parentNode.classList.remove("l-flats__filters--active");
});

$(window).on('load', function (event) {
    event.preventDefault();
    var app = Sammy();
    app.run();
    app.get('(.*)', function () {
        switchScreen({
            path: this.params.splat
        });
        try {
            ga('set', 'page', this.path);
            ga('send', 'pageview');
            yaCounter43790899.hit("http://" + window.location.hostname + this.path);
        } catch (err) {
            console.log(err);
        }
    });

    $(".b-menu__close-btn:first").on('click', function () {
        hideMainMenu();
    });

    setActiveMainMenuLink();

    $("#cloudsOverlay").on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });

    hidePagePreloader();

    setTimeout(function () {
        var $preloaderOverlay = $("#pagePreloader");
        $preloaderOverlay.addClass('b-page-preloader--transparent');
    }, 0.9);

    $(".l-footer").on('selectstart', function (event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });
    new InteractiveForm({ el: "#callMeForm", successTextClass: "b-aside-form__success-text" });
});

function fireTrackersOnSubmit(path) {
    try {
        switch (path) {
            case '/ajax/form-callbackmain/':
                yaCounter43790899.reachGoal('Naglavnoi');
                ga('send', 'event', 'Naglavnoi', 'click1', 'forma1');
                break;
            case "/ajax/form-call/":
                yaCounter43790899.reachGoal('Zakaz_zvonka');
                ga('send', 'event', 'Zakaz_zvonka', 'click2', 'forma2');
                break;
            case "/ajax/form-feedback/":
                yaCounter43790899.reachGoal('Napichi_nam');
                ga('send', 'event', 'Napichi_nam', 'click3', 'forma3');
                break;
            case "/ajax/form-getprice2/":
                yaCounter43790899.reachGoal('Planirovki_usloviya');
                ga('send', 'event', 'Planirovki_usloviya', 'click4', 'forma4');
                break;
            case "/ajax/form-getprice/":
                yaCounter43790899.reachGoal('Planirovki_stoimost');
                ga('send', 'event', 'Planirovki_stoimos', 'click5', 'forma5');
                break;
            case "/ajax/form-caroder/":
                yaCounter43790899.reachGoal('Zakaz_transport');
                ga('send', 'event', 'Zakaz_transport', 'click6', 'forma6');
                break;
            case "/ajax/form-mortgage/":
                yaCounter43790899.reachGoal('Ipoteka');
                ga('send', 'event', 'Ipoteka', 'click7', 'forma7');
                break;

            default:
            // code block
        }
    } catch (e) {
        console.log(e);
    }
}

$.validator.addMethod("regex", function (value, element, regexp) {
    var re = new RegExp(regexp);
    return this.optional(element) || re.test(value);
}, "Поле заполнено не верно");

var InteractiveForm = function () {
    function InteractiveForm(opts) {
        _classCallCheck(this, InteractiveForm);

        this.$form = $(opts.el).eq(0);
        var $form = this.$form;

        var successTextClass = opts.successTextClass || "interactive-form-success-text";
        var successTextTag = opts.successTextTag || "div";

        $form.find('[name="phone"]').inputmask({
            mask: "+7 999 999-9999",
            showMaskOnHover: false
        });
        $form.find('[name="time"]').inputmask("hh:mm  -  hh:mm", {

            showMaskOnHover: false
        });
        $form.find('[name="time_from"],[name="time_to"]').inputmask("hh:mm", {
            showMaskOnHover: false
        });
        $form.find('[name="captcha"]').inputmask({
            mask: "9999",
            showMaskOnHover: false
        });

        var validatorOpts = {
            rules: {
                phone: {
                    required: true,
                    regex: /\+7\s\d\d\d\s\d\d\d\-\d\d\d\d/
                },
                captcha: {
                    required: true,
                    regex: /\d\d\d\d/
                },
                time: {
                    required: true,
                    regex: /\d\d\:\d\d\s\s-\s\s\d\d\:\d\d/
                }
            },
            onfocusout: function onfocusout(el, event) {
                $(el).valid();
            },
            onkeyup: false,
            onclick: false,
            focusCleanup: false,

            submitHandler: function submitHandler(form) {
                var $form = $(form);
                var $btn = $form.find('[type="submit"]:first');
                $btn.attr('disabled', true);
                showPagePreloader();
                $.ajax({
                    url: $form.data('action'),
                    type: 'POST',
                    data: $form.serialize(),
                    dataType: "json"
                }).done(function (data) {
                    var errorCode = parseInt(data.code);

                    if (errorCode === 0) {

                        TweenLite.to($form, 0.2, {
                            scale: 0,
                            ease: Power1.easeIn,
                            onComplete: function onComplete() {
                                if (opts.beforeSuccessTextInsert) {
                                    opts.beforeSuccessTextInsert($form);
                                }
                                $btn.removeAttr('disabled');
                                $form.after('<' + successTextTag + ' class="' + successTextClass + '">' + data.success + '</' + successTextTag + '>').hide().parent().find(".b-aside-form__header:first").hide();
                                if (opts.afterSuccessTextInsert) {
                                    opts.afterSuccessTextInsert($form);
                                }

                                fireTrackersOnSubmit($form.data('action'));
                            }
                        });
                    } else if (errorCode === 1) {
                        var validator = $form.data('validator');
                        $btn.removeAttr('disabled');
                        $form.find('[name="captcha"]:first').trigger('focus');
                        $form.find('img:first').trigger('click');
                        //$form.find('label[for="captcha"]');
                        validator.showErrors({
                            captcha: data.error
                        });
                    }
                }).always(function () {
                    hidePagePreloader();
                });
                return false;
            }
        };

        if (opts.validatorParams) {
            $.extend(true, validatorOpts, opts.validatorParams);
        }

        this.validator = $form.validate(validatorOpts);
    }

    _createClass(InteractiveForm, [{
        key: 'destroy',
        value: function destroy() {
            this.validator.destroy();
            this.$form.find('input').inputmask('remove');
        }
    }]);

    return InteractiveForm;
}();