"use strict";

(function () {
    var $cont = $(".l-infras--internal:first").parent();

    $cont.find(".b-taglist__link").on('click', function (event) {
        event.preventDefault();
        var $tag = $(event.currentTarget);
        var $pins = $(".b-contacts-map__pin");

        $tag.toggleClass('b-taglist__link--active');

        var $activeTags = $(".b-taglist__link--active");
        if (!$activeTags.length) {
            $pins.fadeIn();
        } else {
            var activePinsGroups = [];
            $activeTags.each(function (index, el) {
                activePinsGroups.push(el.dataset.pins);
            });

            var $activePins = $pins.filter(function (index, el) {
                return _.includes(activePinsGroups, el.dataset.pinGroup);
            });

            var $nonActivePins = $pins.filter(function (index, el) {
                return !_.includes(activePinsGroups, el.dataset.pinGroup);
            });

            $activePins.fadeIn();
            $nonActivePins.fadeOut();
        }
    });
})();