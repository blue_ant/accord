"use strict";

(function () {
    document.querySelector(".l-topbar").classList.add("l-topbar--white");

    $(".l-tour-entry:first").parent().on('transitStart', function (event) {
        document.querySelector(".l-topbar").classList.remove("l-topbar--white");
    });
    embedpano({ swf: "pan_01_fin.swf", xml: "/tours/pan_01_fin.xml", target: "pano", id: "pano1", html5: "auto", passQueryParameters: true });

    //embedpano({ swf: "pan_02_fin.swf", xml: "/tours/pan_02_fin.xml", target: "pano", html5: "auto", passQueryParameters: true });
})();