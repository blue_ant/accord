"use strict";

(function () {

    var homepageForm = new InteractiveForm({
        el: ".b-prospage-callback-form__form:first",
        successTextClass: "b-prospage-callback-form__success-text",
        beforeSuccessTextInsert: function beforeSuccessTextInsert($form) {
            $(".b-prospage-callback-form__lead:first").remove();
        },
        validatorParams: {
            highlight: function highlight(element, errorClass, validClass) {
                $(element).addClass('b-prospage-callback-form__input--error').parent().find(".b-prospage-callback-form__label").removeClass(validClass).addClass('b-prospage-callback-form__label--error');
            },
            unhighlight: function unhighlight(element, errorClass, validClass) {
                $(element).removeClass('b-prospage-callback-form__input--error').parent().find(".b-prospage-callback-form__label").removeClass("b-prospage-callback-form__label--error").addClass(validClass).html("введите ваш номер");
            },
            errorPlacement: function errorPlacement(error, element) {
                $(element).parent().find(".b-prospage-callback-form__label").html(error.html());
            }
        }
    });
    var homepageInit = function homepageInit() {
        function setHomepageSlidesProgressBar(percent) {
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 8;

            TweenLite.to(".b-bottom-progressbar__track", duration, {
                width: percent + "%",
                ease: Power0.easeNone
            });
        }

        var $cont = $(".b-homepage-slides:first").parent();
        var timers = [];
        var homepageSlider = new Swiper('.b-homepage-slides', {
            // Optional parameters
            direction: 'horizontal',
            loop: false,
            slidesPerView: 1,
            // Navigation arrows
            //nextButton: '.swiper-button-next',
            //prevButton: '.swiper-button-prev',
            effect: "fade",
            speed: 1000
        });

        //$(".b-homepage-videobg:first").get(0).play();
        var homepageSlidesTimer;
        /*var homepageSlidesTimer = setInterval(function() {
            progressHomepageSlide();
        }, 6900);
        setHomepageSlidesProgressBar(14);*/

        $(".b-homepage-heading__btn:first").on('click', function () {
            clearInterval(homepageSlidesTimer);
            progressHomepageSlide(0);
            homepageSlidesTimer = setInterval(function () {
                progressHomepageSlide();
            }, 6900);
        });

        /*else{
            homepageSlider.slideTo(7, 0, false);
            $(".b-bottom-progressbar:first").hide();
        }*/

        function progressHomepageSlide(slideNum) {
            var currentSlideNum = slideNum || homepageSlider.realIndex;
            if (currentSlideNum === 5) {
                clearInterval(homepageSlidesTimer);
            }
            switch (currentSlideNum) {
                case 0:
                    slide1to2();
                    break;
                case 1:
                    slide2to3();
                    break;
                case 2:
                    slide3to4();
                    break;
                case 3:
                    slide4to5();
                    break;
                case 4:
                    slide5to6();
                    break;
                case 5:
                    slide6to7();
                    break;
            }

            function slide6to7() {

                var $slide6imgs = $(".b-prospage-pics__wrap--5:first .b-prospage-pics__img");
                var $slide6textblock = $(".b-pros--5:first");
                var $slide6pins = $(".b-pinbtn--pros-5-1:first,.b-pinbtn--pros-5-2:first");
                var $slide7form = $(".b-prospage-callback-form");
                //prepare slide #7
                $(".l-footer:first").removeClass('visible-xs');
                //start hiding  slide #6
                TweenMax.staggerTo($slide6pins, 1, {
                    x: -50,
                    opacity: 0
                }, 0.1, function () {
                    TweenLite.to($slide6textblock, 1, {
                        y: 100,
                        opacity: 0
                    });
                    TweenMax.staggerTo($slide6imgs, 1, {
                        y: $(window).height(),
                        opacity: 0,
                        ease: Power2.easeInOut
                    }, 0.2, function () {
                        //switch slides
                        homepageSlider.slideTo(6, 0, false);
                        setHomepageSlidesProgressBar(100);

                        //show slide #7
                        TweenLite.set($slide7form, {
                            visibility: "visible"
                        });
                        TweenLite.from($slide7form, 1, {
                            opacity: 0,
                            ease: Power2.easeInOut,
                            scaleY: 0.5,
                            scaleX: 0.5,
                            onComplete: function onComplete() {
                                //reset slide #6
                                TweenLite.set([$slide6imgs, $slide6textblock, $slide6pins], {
                                    clearProps: "visibility,opacity,x,y"
                                });
                            }
                        });
                    });
                });
            }

            function slide5to6() {
                var $slide5imgs = $(".b-prospage-pics__wrap--4:first .b-prospage-pics__img");
                var $slide5textblock = $(".b-pros--4:first");
                var $slide5pin = $(".b-pinbtn--pros-4-1:first");
                var $slide6imgs = $(".b-prospage-pics__wrap--5:first .b-prospage-pics__img");
                var $slide6textblock = $(".b-pros--5:first");
                var $slide6pins = $(".b-pinbtn--pros-5-1:first,.b-pinbtn--pros-5-2:first");
                //prepare slide #6
                TweenLite.set([$slide6imgs, $slide6textblock, $slide6pins], {
                    visibility: "hidden"
                });
                //start hiding  slide #5
                TweenLite.to($slide5pin, 0.5, {
                    opacity: 0,
                    y: 50,
                    onComplete: function onComplete() {
                        TweenLite.to($slide5textblock, 1, {
                            y: 100,
                            opacity: 0
                        });
                        TweenMax.staggerTo($slide5imgs, 1, {
                            y: $(window).height(),
                            opacity: 0,
                            ease: Power2.easeIn
                        }, 0.1, function () {
                            //switch slides
                            homepageSlider.slideTo(5, 0, false);
                            setHomepageSlidesProgressBar(86);
                            //show slide #6
                            TweenLite.set([$slide6imgs, $slide6textblock], {
                                clearProps: "visibility"
                            });
                            TweenLite.from($slide6textblock, 1, {
                                opacity: 0,
                                x: -100,
                                ease: Power2.easeOut
                            });
                            TweenMax.staggerFrom($slide6imgs, 1, {
                                y: -$(window).height(),
                                opacity: 0,
                                ease: Power2.easeOut
                            }, 0.1, function () {
                                TweenLite.set($slide6pins, {
                                    clearProps: "visibility"
                                });
                                TweenMax.staggerFrom($slide6pins, 1, {
                                    y: -50,
                                    opacity: 0
                                }, 0.1, function () {
                                    //reset slide #5
                                    TweenLite.set([$slide5imgs, $slide5textblock, $slide5pin], {
                                        clearProps: "visibility,opacity,x,y"
                                    });
                                });
                            });
                        });
                    }
                });
            }

            function slide4to5() {
                var $slide4img = $(".b-prospage-pics__wrap--3:first .b-prospage-pics__img");
                var $slide4textblock = $(".b-pros--3:first");
                var $slide4linePins = $(".b-pinbtn--pros-3-1:first,.b-pinbtn--pros-3-2:first");
                var $slide4innerPin = $(".b-pinbtn--pros-3-3:first");
                var $slide5imgs = $(".b-prospage-pics__wrap--4:first .b-prospage-pics__img");
                var $slide5textblock = $(".b-pros--4:first");
                var $slide5pin = $(".b-pinbtn--pros-4-1:first");
                //prepare slide #5
                TweenLite.set([$slide5imgs, $slide5textblock, $slide5pin], {
                    visibility: "hidden"
                });
                //start hiding  slide #4
                TweenLite.to($slide4innerPin, 1, {
                    scaleX: 0.6,
                    scaleY: 0.6,
                    opacity: 0
                });
                TweenMax.staggerTo($slide4linePins, 1, {
                    delay: 0.1,
                    x: -80,
                    opacity: 0
                }, 0.3, function () {
                    TweenLite.to($slide4img, 1, {
                        opacity: 0,
                        x: parseInt($slide4img.width() / 2),
                        ease: Power2.easeIn
                    });
                    TweenLite.to($slide4textblock, 1, {
                        x: -100,
                        opacity: 0,
                        ease: Power2.easeIn,
                        onComplete: function onComplete() {
                            //switch slides
                            homepageSlider.slideTo(4, 0, false);
                            setHomepageSlidesProgressBar(72);
                            //show slide #5
                            TweenLite.set($slide5imgs, {
                                visibility: "visible"
                            });
                            TweenMax.staggerFrom($slide5imgs, 1, {
                                y: -$(window).height(),
                                opacity: 0,
                                ease: Power2.easeInOut
                            }, 0.1, function () {
                                TweenLite.set($slide5pin, {
                                    clearProps: "opacity,visibility"
                                });
                                TweenMax.from($slide5pin, 0.4, {
                                    opacity: 0,
                                    y: 50
                                });
                                TweenLite.set($slide5textblock, {
                                    clearProps: "opacity,visibility"
                                });
                                TweenLite.from($slide5textblock, 1, {
                                    x: -100,
                                    opacity: 0
                                });
                            });
                            //reset slide #4
                            TweenLite.set([$slide4img, $slide4textblock, $slide4linePins, $slide4innerPin], {
                                clearProps: "opacity,visibility,x,y"
                            });
                        }
                    });
                });
            }

            function slide3to4() {
                var $slide3imgs = $(".b-prospage-pics__wrap--2:first .b-prospage-pics__img");
                var $slide3textblock = $(".b-pros--2:first");
                var $slide3pin = $(".b-pinbtn--pros-2-1:first");
                var $slide4img = $(".b-prospage-pics__wrap--3:first .b-prospage-pics__img");
                var $slide4textblock = $(".b-pros--3:first");
                var $slide4linePins = $(".b-pinbtn--pros-3-1:first,.b-pinbtn--pros-3-2:first");
                var $slide4innerPin = $(".b-pinbtn--pros-3-3:first");
                //prepare slide #4
                TweenLite.set([$slide4img, $slide4textblock, $slide4linePins, $slide4innerPin], {
                    visibility: "hidden"
                });
                //start hiding  slide #3
                TweenLite.to($slide3pin, 0.8, {
                    opacity: 0,
                    y: 34,
                    onComplete: function onComplete() {
                        TweenLite.to($slide3textblock, 1, {
                            opacity: 0,
                            y: 150
                        });
                        TweenMax.staggerTo($slide3imgs, 1, {
                            opacity: 0,
                            y: $(window).height(),
                            ease: Power2.easeIn
                        }, 0.1, function () {
                            //switch slides
                            homepageSlider.slideTo(3, 0, false);
                            setHomepageSlidesProgressBar(58);
                            //show slide #4
                            TweenLite.set([$slide4textblock, $slide4img], {
                                visibility: "visible"
                            });
                            TweenLite.from($slide4textblock, 1, {
                                x: -80,
                                opacity: 0
                            });
                            TweenLite.from($slide4img, 1, {
                                x: $slide4img.width(),
                                opacity: 0,
                                onComplete: function onComplete() {
                                    TweenLite.set($slide4linePins, {
                                        visibility: "visible"
                                    });
                                    TweenMax.staggerFrom($slide4linePins, 1, {
                                        x: -60,
                                        opacity: 0
                                    }, 0.2, function () {
                                        TweenLite.set($slide4innerPin, {
                                            visibility: "visible"
                                        });
                                        TweenLite.from($slide4innerPin, 0.5, {
                                            opacity: 0,
                                            onComplete: function onComplete() {
                                                //reset slide #3
                                                TweenLite.set([$slide3imgs, $slide3textblock, $slide3pin], {
                                                    clearProps: "opacity,visibility,y"
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            }

            function slide2to3() {
                var $slide2img = $(".b-prospage-pics--slide-1:first .b-prospage-pics__img:first");
                var $slide2textblock = $(".b-pros--1:first");
                var $slide2pins = $(".b-pinbtn--pros-1-1:first, .b-pinbtn--pros-1-2:first");
                var $slide3imgs = $(".b-prospage-pics__wrap--2:first .b-prospage-pics__img");
                var $slide3textblock = $(".b-pros--2:first");
                var $slide3pin = $(".b-pinbtn--pros-2-1:first");
                //prepare slide #3
                TweenLite.set([$slide3imgs, $slide3textblock, $slide3pin], {
                    visibility: 'hidden'
                });
                //start hiding  slide #2
                TweenMax.staggerTo($slide2pins, 1, {
                    y: 80,
                    opacity: 0,
                    onComplete: function onComplete() {
                        TweenLite.to($slide2textblock, 1, {
                            y: 100,
                            opacity: 0
                        });
                        TweenLite.to($slide2img, 1, {
                            y: parseInt($(window).height() / 4),
                            opacity: 0
                        });
                    }
                }, 0.2);
                var timer1 = setTimeout(function () {
                    homepageSlider.slideTo(2);
                    setHomepageSlidesProgressBar(44);
                    //show slide #3
                    TweenLite.set($slide3imgs, {
                        visibility: "visible"
                    });
                    TweenMax.staggerFrom($slide3imgs, 1, {
                        y: -1 * $(window).height(),
                        opacity: 0
                    }, 0.1);
                    TweenLite.set($slide3textblock, {
                        visibility: "visible"
                    });
                    TweenLite.from($slide3textblock, 1, {
                        y: -90,
                        opacity: 0,
                        delay: 0.5,
                        ease: Power1.easeIn,
                        onComplete: function onComplete() {
                            TweenLite.set($slide3pin, {
                                visibility: "visible"
                            });
                            TweenLite.from($slide3pin, 0.6, {
                                opacity: 0,
                                ease: Power1.easeIn,
                                onComplete: function onComplete() {
                                    //reset slide #2
                                    TweenLite.set([$slide2img, $slide2textblock, $slide2pins], {
                                        clearProps: "opacity,visibility,y"
                                    });
                                }
                            });
                        }
                    });
                }, 1800);

                timers.push(timer1);
            }

            function slide1to2() {
                $(".l-footer:first").addClass('visible-xs');
                $("#homepageSpecialFooterStyles").remove();
                var $headers = $(".b-homepage-heading:first");
                var $actionList = $(".b-homepage-actions:first");
                var $slide2img = $(".b-prospage-pics--slide-1:first .b-prospage-pics__img:first");
                var $slide2textblock = $(".b-pros--1:first");
                var $slide2pins = $(".b-pinbtn--pros-1-1:first, .b-pinbtn--pros-1-2:first");
                //prepare slide #2
                TweenLite.set([$slide2img, $slide2textblock, $slide2pins], {
                    visibility: "hidden"
                });
                //start hiding  slide #1
                TweenLite.to($actionList, 1, {
                    opacity: 0
                });
                TweenLite.to($headers, 1, {
                    opacity: 0,
                    y: 90
                });
                //switch slides
                var timer2 = setTimeout(function () {
                    homepageSlider.slideNext();
                    setHomepageSlidesProgressBar(28);
                }, 1000);
                timers.push(timer2);

                //show slide #2
                var timer3 = setTimeout(function () {
                    TweenLite.set([$slide2img, $slide2textblock], {
                        visibility: "visible"
                    });
                    TweenLite.from($slide2img, 1, {
                        y: -1 * $(window).height(),
                        opacity: 0,
                        ease: Power1.easeOut
                    });
                    TweenLite.from($slide2textblock, 1, {
                        y: -100,
                        opacity: 0,
                        onComplete: function onComplete() {
                            TweenLite.set($slide2pins, {
                                visibility: "visible"
                            });
                            TweenMax.staggerFrom($slide2pins, 1, {
                                opacity: 0,
                                y: -90
                            }, 0.2);
                        }
                    });
                }, 2000);
                timers.push(timer3);

                //reset slide #1
                var timer4 = setTimeout(function () {
                    TweenLite.set([$headers, $actionList], {
                        clearProps: "opacity,y"
                    });
                }, 2000);
                timers.push(timer4);
            }
        }

        $('.b-logo').on('click.homepage', function (event) {
            event.preventDefault();
            if (homepageSlider.realIndex !== 0) {
                var video = $(".b-homepage-videobg:first").get(0);
                $cont.append("<style id=\"homepageSpecialFooterStyles\">\n        @media(min-width:768px) {\n            .l-footer--main .b-footer__pic{color: #fff;}\n            #whitePageFooterBuilderLogo{opacity:1;}\n            #grayPageFooterBuilderLogo{opacity:0;}\n    }\n</style>\n");
                //video.play();
                //video.pause();
                $(".l-footer:first").removeClass('visible-xs');
                clearInterval(homepageSlidesTimer);
                homepageSlider.slideTo(0);
                setHomepageSlidesProgressBar(0, 0.5);
            }

            return false;
        });

        $cont.on('transitStart', function (event) {
            $('.b-logo').off('.homepage');
        }).on('transitEnd', function (event) {

            $(".l-footer:first").removeClass('visible-xs');
            clearInterval(homepageSlidesTimer);
            for (var i = timers.length - 1; i >= 0; i--) {
                clearTimeout(timers[i]);
                timers[i] = null;
            }
            homepageSlider.destroy(true, true);
            homepageForm.destroy();
            homepageSlider = null;
            homepageSlidesTimer = null;
            timers = null;
        });
    };

    if (document.readyState === "complete") {
        homepageInit();
    } else {
        $(window).one('load', function () {
            homepageInit();
            setTimeout(function () {
                $(".b-homepage-heading__btn:first").trigger('click');
            }, 5000);
        });
    }
})();