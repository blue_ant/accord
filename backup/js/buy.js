"use strict";

(function () {

    var $cont = $(".l-buy:first").parent();
    var writeUsForm = new InteractiveForm({
        el: $cont.find("#form1"),
        successTextClass: "b-aside-form__success-text"
    });

    $cont.on('transitStart', function (event) {
        writeUsForm.destroy();
    }).on('transitEnd', function (event) {});
})();