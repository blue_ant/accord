"use strict";

(function () {
    insertGenplanModal();
    var handlersID = "singleFlatPage" + _.random(1, 10000);

    var starredFlatsModal = insertFavoritPlansPopup();

    var $starredFlatsModal = starredFlatsModal.modal;
    var $miniplansWrap = starredFlatsModal.miniplansWrap;

    refreshFavoritPlansList();

    var $genplanModal = $("#genplanModal");
    var $conts = $(".l-single-flat");
    var $cont = $conts.eq($conts.length - 1).parent();
    var $flatsListCont = $cont.find('#flastListTableContainer');
    var $schemeModal = $("#floorSchemeModal");

    var scroller1 = $cont.find('.b-single-flat:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#singFlatPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    $(document).on('click.' + handlersID, '.b-flats-filter__apply-btn', function (event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    });

    var rangeSlider1 = $cont.find('#singleFlatAreaRangeInput:first').slider({
        tooltip: 'hide'
    }).on("slide", function (event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#singleFlatAreaMin").html(min);
        $("#singleFlatAreaMax").html(max);

        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    var $releaseSliderInp = $cont.find('#singleFlatCompletenessRangeInput:first');
    var firstRelease = new Date($releaseSliderInp.data("slider-min") * 1000);
    var lastRelease = new Date($releaseSliderInp.data("slider-max") * 1000);

    var quarterPoints = getQuartersFromDateRange(firstRelease, lastRelease);

    var interval = 31 * 3 * 24 * 60 * 60;
    var rangeSlider2 = $releaseSliderInp.slider({
        tooltip: 'hide',
        ticks: quarterPoints,
        ticks_snap_bounds: interval,
        min: quarterPoints[0],
        max: _.last(quarterPoints),
        step: interval
    }).on("slide", function (event) {
        var min = event.value[0];
        var max = event.value[1];
        $cont.find('[name="finish_from"]:first').val(min);
        $cont.find('[name="finish_to"]:first').val(max);
        var fmtTpl = 'Qкв YYYY';
        if (Date.now() / 1000 > min) {
            $("#singleFlatCompletenessDateMin").text("Готово");
        } else {
            $("#singleFlatCompletenessDateMin").text(moment.unix(min).format(fmtTpl));
        }

        $("#singleFlatCompletenessDateMax").text(moment.unix(max).format(fmtTpl));
    });

    rangeSlider2.slider("setValue", rangeSlider2.slider("getValue"), true);

    var flatsTableTpl = Handlebars.compile("\n\n{{#if flats}}\n<section class=\"b-flats-list hidden-xs\">\n    <table class=\"b-flats-list__table table\">\n    <caption class=\"b-flats-list__caption\">\u041A\u0432\u0430\u0440\u0442\u0438\u0440\u044B \u0432\u044B\u0431\u0440\u0430\u043D\u043D\u043E\u0439 \u043F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0438</caption>\n    <tbody> \n        {{#each flats}}\n        <tr>\n            <td class=\"b-flats-list__td b-flats-list__td--n1\">\u2116{{num}}</td>\n            <td class=\"b-flats-list__td\">\u043A\u043E\u0440\u043F\u0443\u0441 <b class=\"text-primary\">{{house}}</b></td>\n            <td class=\"b-flats-list__td\">\u0441\u0435\u043A\u0446\u0438\u044F <b class=\"text-primary\">{{section}}</b></td>\n            <td class=\"b-flats-list__td\">\u044D\u0442\u0430\u0436 <b class=\"text-primary\">{{floor}}</b></td>\n            <td class=\"b-flats-list__td\">{{#if ../typical.bedrooms}}\u0441\u043F\u0430\u043B\u0435\u043D <b class=\"text-primary\">{{../typical.bedrooms}}</b>{{else}}\u0441\u0442\u0443\u0434\u0438\u044F{{/if}}</td>\n            <td class=\"b-flats-list__td\">\u043E\u0431\u0449\u0430\u044F \u043F\u043B\u043E\u0449\u0430\u0434\u044C <b class=\"text-primary\">{{area}} \u043C<sup>2</sup></b></td>\n            <td class=\"b-flats-list__td\"><u data-plan-img=\"{{plan_floor}}\" data-plan-section=\"{{section}}\">\u043D\u0430 \u044D\u0442\u0430\u0436\u0435</u></td>\n            <td class=\"b-flats-list__td\">\n                <div class=\"b-flats-list__icon b-flats-list__icon--brush{{#if furnish}} b-flats-list__icon--active{{/if}}\" data-toggle=\"popover\" data-content=\"{{#if furnish}}\u0441 \u043E\u0442\u0434\u0435\u043B\u043A\u043E\u0439{{else}}\u0431\u0435\u0437&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u0438{{/if}}\"></div>\n            </td>\n            <td class=\"b-flats-list__td\">\n                <div class=\"b-flats-list__icon b-flats-list__icon--percent{{#if ../typical.balcony}} b-flats-list__icon--active{{/if}}\" data-toggle=\"popover\" data-content=\"{{#if ../typical.balcony}}\u0441&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u043E\u043C{{else}}\u0431\u0435\u0437 \u0431\u0430\u043B\u043A\u043E\u043D\u0430{{/if}}\"></div>\n            </td>\n        </tr>\n        {{/each}}\n    </tbody>\n    </table>\n</section>\n<section class=\"b-mobile-flats-list visible-xs\">\n    <table class=\"b-mobile-flats-list__table table\">\n        <caption class=\"b-mobile-flats-list__caption\">\u041A\u0432\u0430\u0440\u0442\u0438\u0440\u044B \u0432\u044B\u0431\u0440\u0430\u043D\u043D\u043E\u0439 \u043F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0438</caption>\n        <tbody> {{#each flats}}\n            <tr>\n                <td class=\"b-mobile-flats-list__td\">\n                    <div class=\"b-mobile-flats-list__value\">\u2116{{num}}</div> \u043A\u0432\u0430\u0440\u0442\u0438\u0440\u0430</td>\n                <td class=\"b-mobile-flats-list__td\">\n                    <div class=\"b-mobile-flats-list__value\">{{house}}</div> \u043A\u043E\u0440\u043F\u0443\u0441 </td>\n                <td class=\"b-mobile-flats-list__td\">\n                    <div class=\"b-mobile-flats-list__value\">{{floor}}</div> \u044D\u0442\u0430\u0436 </td>\n                <td class=\"b-mobile-flats-list__td\">\n                    <div class=\"b-mobile-flats-list__value\">{{area}} \u043C<sup>2</sup></div> \u043F\u043B\u043E\u0449\u0430\u0434\u044C </td>\n                <td class=\"b-mobile-flats-list__td\">\n                    <div class=\"b-mobile-flats-list__icon b-mobile-flats-list__icon--brush{{#if furnish}} b-mobile-flats-list__icon--active{{/if}}\" data-toggle=\"popover\" data-content=\"{{#if furnish}}\u0441 \u043E\u0442\u0434\u0435\u043B\u043A\u043E\u0439{{else}}\u0431\u0435\u0437&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u0438{{/if}}\"></div>\n                    <div class=\"b-mobile-flats-list__icon b-mobile-flats-list__icon--percent{{#if ../typical.balcony}} b-mobile-flats-list__icon--active{{/if}} data-toggle=\"popover\" data-content=\"{{#if ../typical.balcony}}\u0441&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u043E\u043C{{else}}\u0431\u0435\u0437 \u0431\u0430\u043B\u043A\u043E\u043D\u0430{{/if}}\"></div>\n                </td>\n            </tr>{{/each}}\n        </tbody>\n    </table>\n</section>\n{{else}}\n\n<h3 style=\"margin-top: 86px;\">\u041D\u0435\u0442 \u043A\u0432\u0430\u0440\u0442\u0438\u0440 \u043F\u043E \u0432\u0430\u0448\u0438\u043C \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u0430\u043C.</h3>\n<h5>\u041F\u043E\u043F\u0440\u043E\u0431\u0443\u0439\u0442\u0435 \u0438\u0437\u043C\u0435\u043D\u0438\u0442\u044C \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u044B \u0444\u0438\u043B\u044C\u0442\u0440\u043E\u0432.</h5>\n\n{{/if}}\n\n<div style=\"position:absolute;height:0;width:0;visibility:hidden;background:{{#each flats}} url('{{plan_floor}}'),{{/each}}transparent;\"></div>\n");

    $flatsListCont.on('mousedown.' + handlersID, "[data-plan-img]", function (event) {
        event.preventDefault();
        var link = event.currentTarget;
        $schemeModal.find('.b-floor-scheme__img:first').css('background-image', "url(" + link.dataset.planImg + ")");
        $schemeModal.find('.b-floor-scheme__title:first').html("\u0421\u0435\u043A\u0446\u0438\u044F <span class='text-primary'>" + link.dataset.planSection + "</span>");
    }).on('click.' + handlersID, '[data-plan-img]', function (event) {
        event.preventDefault();
        setTimeout(function () {
            $schemeModal.modal("show");
        }, 200);
    });

    var flatID = parseInt(window.location.href.split("typical-")[1]);
    var $filterForm = $cont.find("#formFilterSingleApartment");

    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage"
    });

    $filterForm.find("input").on("change", _.debounce(function () {
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function (event) {
        event.preventDefault();
        showPagePreloader();
        var $nonEmptyInputs = $filterForm.find("input").filter(function (index, element) {
            return $(element).val() !== "";
        });

        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage"
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }

        $.ajax({
            url: '/apartments/?ajax=1&get_flats=' + flatID + urlParams

        }).done(function (data) {
            $flatsListCont.find(".b-flats-list__icon,.b-mobile-flats-list__icon").popover("destroy");

            var markup = flatsTableTpl(data);
            $flatsListCont.html(markup);

            hidePagePreloader();

            $flatsListCont.find(".b-flats-list__icon,.b-mobile-flats-list__icon").popover({
                trigger: "hover",
                placement: "top"
            });
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.find('.b-single-flat__icon').popover({
        html: true,
        trigger: "hover"
    });

    $cont.find(".b-single-flat__title-star:first").on('click', function (event) {
        event.preventDefault();
        /*let $star = $(this);
        $star.toggleClass('b-single-flat__title-star--active');*/
        toggleFavoriteFlat(this.dataset.flatId);
    });

    var $asideForm = $cont.find("#form7");
    var getFlatForm = new InteractiveForm({ el: $asideForm, successTextClass: "b-aside-form__success-text" });

    $cont.on('transitStart', function (event) {
        genplanHide();
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document, $flatsListCont]).off("." + handlersID);
        $cont.find('.b-single-flat__icon,.b-flats-list__icon,.b-mobile-flats-list__icon').popover('destroy');
    }).on('transitEnd', function (event) {

        scroller1.remove();
        scroller2.remove();

        rangeSlider1.slider('destroy');
        rangeSlider2.slider('destroy');

        getFlatForm.destroy();
        getFlatForm = null;
    });
})();