"use strict";

(function () {
    var $cont = $(".l-docs:first");
    var $col = $cont.find('.b-centered-text:first');
    $col.niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }
    });
})();