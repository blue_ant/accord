"use strict";

(function () {

    insertGenplanModal();

    var starredFlatsModal = insertFavoritPlansPopup();

    var $starredFlatsModal = starredFlatsModal.modal;
    var $miniplansWrap = starredFlatsModal.miniplansWrap;

    refreshFavoritPlansList();

    var $genplanModal = $("#genplanModal");
    var $cont = $(".l-flats:first").parent();
    var scroller1 = $cont.find('.b-flats:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#allFlatsPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    $(document).on('click.flatspage', '.b-flats-filter__apply-btn', function (event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    }).on('click.flatspage', '.b-flatcard__star .b-flatcard__iconbtn', function (event) {
        event.preventDefault();
        var link = event.currentTarget;
        toggleFavoriteFlat(link.dataset.flatId);
    });

    var rangeSlider1 = $cont.find('#areaRangeInput:first').slider({
        tooltip: 'hide'
    }).on("slide", function (event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#areaMin").html(min);
        $("#areaMax").html(max);

        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    var $releaseSliderInp = $cont.find('#completenessRangeInput:first');
    var firstRelease = new Date($releaseSliderInp.data("slider-min") * 1000);
    var lastRelease = new Date($releaseSliderInp.data("slider-max") * 1000);

    var quarterPoints = getQuartersFromDateRange(firstRelease, lastRelease);

    var interval = 31 * 3 * 24 * 60 * 60;
    var rangeSlider2 = $releaseSliderInp.slider({
        tooltip: 'hide',
        ticks: quarterPoints,
        ticks_snap_bounds: interval,
        min: quarterPoints[0],
        max: _.last(quarterPoints),
        step: interval
    }).on("slide", function (event) {
        var min = event.value[0];
        var max = event.value[1];
        $cont.find('[name="finish_from"]:first').val(min);
        $cont.find('[name="finish_to"]:first').val(max);
        var fmtTpl = 'Qкв YYYY';
        if (Date.now() / 1000 > min) {
            $("#completenessDateMin").text("Готово");
        } else {
            $("#completenessDateMin").text(moment.unix(min).format(fmtTpl));
        }

        /* if (min === max) {
          $("#completenessDateMax").html("");
        }else{
          $("#completenessDateMax").html(moment.unix(max).format(fmtTpl));
        }*/

        $("#completenessDateMax").text(moment.unix(max).format(fmtTpl));
    });

    rangeSlider2.slider("setValue", rangeSlider2.slider("getValue"), true);

    var flatCellTpl = Handlebars.compile("\n\n{{#if typicals}}\n{{#each typicals}}\n<div class=\"b-flats__cell col-xs-12 col-sm-6 col-md-4\">\n   <div class=\"b-flats__cell-inner\">\n       <div class=\"b-flatcard hidden-xs\">\n           <div class=\"b-flatcard__row\">\n               <div class=\"b-flatcard__cell\">\n                   <div class=\"va\">\n                       <div class=\"va-item b-flatcard__title\">{{name}}</div>\n                       <div class=\"va-item\">\n                           <div class=\"b-flatcard__star\">\n                               <div class=\"va-item\">\n                                   <a href=\"#\" data-flat-id=\"{{id}}\" class=\"b-flatcard__iconbtn{{#if favorite}} b-flatcard__iconbtn--active{{/if}}\"> <img src=\"/img/star-primary.svg\" class=\"b-flatcard__icon\" alt=\"\"> </a>\n                               </div>\n                           </div>\n                       </div>\n                   </div>\n               </div>\n           </div>\n           <div class=\"b-flatcard__row b-flatcard__row--middle\">\n               <div class=\"b-flatcard__cell\">\n                   <a href=\"{{url}}\" class=\"b-flatcard__img\" style=\"background-image: url({{img}});\"></a>\n               </div>\n           </div>\n           <div class=\"b-flatcard__row\">\n               <div class=\"b-flatcard__cell\">\n                   <div class=\"va\">\n                       <div class=\"va-item b-flatcard__footage\">\n                        {{#if_eq area_min area_max}}\n                          {{area_min}} m<sup>2</sup>\n                        {{else}}\n                          {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>\n                        {{/if_eq}}\n                       </div>\n                       <div class=\"va-item\">\n                           <a href=\"#\" class=\"b-flatcard__iconbtn\" data-toggle=\"popover\" data-content=\"{{#if balcony}}\u0441&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u043E\u043C{{else}}\u0431\u0435\u0437&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u0430{{/if}}\"> <img src=\"/img/balcony-primary.svg\" class=\"b-flatcard__icon{{#if balcony}} b-flatcard__icon--active{{/if}}\" alt=\"\"> </a>\n                       </div>\n                       <div class=\"va-item\">\n                           <a href=\"#\" class=\"b-flatcard__iconbtn\" data-toggle=\"popover\" data-content=\"{{#if furnish}}\u0441&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u043E\u0439{{else}}\u0431\u0435\u0437&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u0438{{/if}}\"> <img src=\"/img/paintbrush-primary.svg\" class=\"b-flatcard__icon{{#if furnish}} b-flatcard__icon--active{{/if}}\" alt=\"\"> </a>\n                       </div>\n                   </div>\n               </div>\n           </div>\n       </div>\n       <div class=\"b-mobile-flatcard container-fluid visible-xs\">\n           <div class=\"row\">\n               <div class=\"col-xs-7\"> <a href=\"{{url}}\"><img src=\"{{img}}\" alt=\"\" class=\"b-mobile-flatcard__img img-responsive center-block\"></a> </div>\n               <div class=\"col-xs-5\">\n                   <div class=\"b-mobile-flatcard__title\">{{name}}</div>\n                   <div class=\"b-mobile-flatcard__footage\">\n                   {{#if_eq area_min area_max}}\n                      {{area_min}} m<sup>2</sup>\n                    {{else}}\n                      {{area_min}} m<sup>2</sup> - {{area_max}} m<sup>2</sup>\n                    {{/if_eq}}\n                    </div>\n                   <div class=\"b-mobile-flatcard__icons\">\n                       <div class=\"b-mobile-flatcard__iconwrap\">\n                           <a href=\"#\" class=\"b-mobile-flatcard__iconbtn\" data-toggle=\"popover\" data-content=\"{{#if furnish}}\u0441&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u043E\u0439{{else}}\u0431\u0435\u0437&nbsp;\u043E\u0442\u0434\u0435\u043B\u043A\u0438{{/if}}\"> <img src=\"/img/paintbrush-primary.svg\" class=\"b-mobile-flatcard__icon{{#if furnish}} b-mobile-flatcard__icon--active{{/if}}\" alt=\"\"> </a>\n                       </div>\n                       <div class=\"b-mobile-flatcard__iconwrap text-center\">\n                           <a href=\"#\" class=\"b-mobile-flatcard__iconbtn\" data-toggle=\"popover\" data-content=\"{{#if balcony}}\u0441&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u043E\u043C{{else}}\u0431\u0435\u0437&nbsp;\u0431\u0430\u043B\u043A\u043E\u043D\u0430{{/if}}\"> <img src=\"/img/balcony-primary.svg\" class=\"b-mobile-flatcard__icon{{#if balcony}} b-mobile-flatcard__icon--active{{/if}}\" alt=\"\"> </a>\n                       </div>\n                   </div>\n               </div>\n           </div>\n       </div>\n   </div>\n</div>\n{{/each}}\n{{else}}\n\n<div class=\"b-flats__empty\">\n    <div class=\"b-flats__empty-inner\">\n        <img src=\"/img/cloud.svg\" alt=\"\" class=\"img-responsive\">\n        <div class=\"b-flats__empty-header\">\u041D\u0415\u0422 \u0420\u0415\u0417\u0423\u041B\u042C\u0422\u0410\u0422\u041E\u0412</div>\n        <div class=\"b-flats__empty-text\">\u041F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0438 \u043F\u043E \u0437\u0430\u0434\u0430\u043D\u043D\u044B\u043C \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u0430\u043C \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B,<br>\n\u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u0438\u0437\u043C\u0435\u043D\u0438\u0442\u0435 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u044B \u043F\u043E\u0438\u0441\u043A\u0430.</div>\n        <img src=\"/img/crane.svg\" alt=\"\" class=\"b-flats__empty-bootom-img\">\n    </div>\n</div>\n\n{{/if}}\n\n<div class=\"col-xs-12 hidden-xs\" style=\"padding-top: 20px;\">&nbsp;</div>\n\n");

    var $filterForm = $cont.find("#formFilterApartments");

    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage"
    });

    $filterForm.find("input").on("change", _.debounce(function () {
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function (event) {
        showPagePreloader();
        event.preventDefault();
        var $nonEmptyInputs = $filterForm.find("input").filter(function (index, element) {
            return $(element).val() !== "";
        });

        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage"
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }
        $.ajax({
            url: '/apartments/?get_typicals=1&ajax=1&filter=1' + urlParams

        }).done(function (data) {

            var markup = flatCellTpl(data);
            var $cellsCont = $cont.find("#flatsCellsContainer");
            $cellsCont.html(markup);

            $cellsCont.find(".b-flatcard__iconbtn,.b-mobile-flatcard__iconbtn").popover({
                trigger: "hover",
                placement: "top"
            });
            hidePagePreloader();
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.on('transitStart', function (event) {
        genplanHide();
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document]).off(".flatspage");
        $cont.find(".b-flatcard__iconbtn,.b-mobile-flatcard__iconbtn").popover("destroy");
    }).on('transitEnd', function (event) {

        rangeSlider1.slider('destroy');
        rangeSlider2.slider('destroy');

        scroller1.remove();
        scroller2.remove();
    });
})();