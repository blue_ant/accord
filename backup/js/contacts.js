"use strict";

(function () {

    var $cont = $(".l-contacts:first").parent();

    var $mobileMap = $cont.find(".b-mobile-fullscreen-map:first");
    $mobileMap.appendTo(document.body);

    $cont.find(".b-contacts__text-toggler").on('click', function (event) {
        event.preventDefault();
        $(this).toggleClass('b-contacts__text-toggler--active').parents(".b-contacts__col:first").find('.b-contacts__content-block:first').slideToggle();
    });
    var orderTransportForm = new InteractiveForm({ el: $cont.find("#form2"), successTextClass: "b-aside-form__success-text" });
    var writeUsForm = new InteractiveForm({ el: $cont.find("#form1"), successTextClass: "b-aside-form__success-text" });

    var accordBuildingsCoords = {
        lat: 55.607676,
        lng: 37.104740
    };

    var map = new google.maps.Map(document.getElementById('contactsMap'), {
        zoom: 12,
        center: { lat: accordBuildingsCoords.lat + 0.03, lng: accordBuildingsCoords.lng + 0.1 },
        streetViewControl: false

    });

    var image = {
        url: "/img/contacts/logo_contact.png",
        scaledSize: new google.maps.Size(134, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(100, 100)

    };

    var marker = new google.maps.Marker({
        position: accordBuildingsCoords,
        map: map,
        icon: image
    });

    var mobileMap = new google.maps.Map(document.getElementById('mobileContactsMap'), {
        zoom: 8,
        center: accordBuildingsCoords,
        mapTypeControl: false,
        streetViewControl: false,
        fullScreenControl: false
    });

    var directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(mobileMap);
    var directionsService = new google.maps.DirectionsService();

    function showMobileContactsMap() {
        $("body").addClass('overflow-hidden-xs');
        $(".b-mobile-fullscreen-map:first").addClass('b-mobile-fullscreen-map--active');

        google.maps.event.trigger(mobileMap, 'resize');
        mobileMap.setCenter(accordBuildingsCoords);
    }

    function hideMobileContactsMap() {
        $("body").removeClass('overflow-hidden-xs');
        $(".b-mobile-fullscreen-map:first").removeClass('b-mobile-fullscreen-map--active');
        mobileMap.setZoom(8);
    }

    function bindMapRouteRenderOnClick(mapInstance, travelMode) {
        directionsDisplay.setMap(null);
        google.maps.event.clearListeners(mapInstance, 'click');
        mapInstance.setOptions({
            draggableCursor: 'pointer'
        });
        google.maps.event.addListener(mapInstance, 'click', function (event) {

            var request = {
                origin: event.latLng,
                destination: accordBuildingsCoords,
                travelMode: google.maps.TravelMode[travelMode]
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(mapInstance);
                } else {
                    alert("Невозможно проложить маршрут из этой точки.");
                }
            });

            return false;
        });

        alert('Отметьте Ваше текущее местоположение на карте');
    }

    $(window).on('resize.contactsPage orientationchange.contactsPage', _.debounce(function () {

        if ($(window).width() >= 768) {
            google.maps.event.trigger(map, 'resize');
            map.setCenter({
                lat: accordBuildingsCoords.lat + 0.03,
                lng: accordBuildingsCoords.lng + 0.1
            });
        } else {
            google.maps.event.trigger(mobileMap, 'resize');
            mobileMap.setCenter(accordBuildingsCoords);
        }
    }, 100));

    function showScheme() {
        var block = document.getElementById('roadScheme');
        var track = block.getElementsByClassName('b-scheme__img-wrap')[0];
        TweenLite.to(block, 0.4, {
            autoAlpha: 1,
            onComplete: function onComplete() {

                TweenLite.to(track, 0.4, { x: "0%", ease: Power1.easeInOut });
            }
        });
    }

    function hideScheme() {
        var block = document.getElementById('roadScheme');
        var track = block.getElementsByClassName('b-scheme__img-wrap')[0];
        TweenLite.to(track, 0.4, {
            x: "100%",
            ease: Power1.easeInOut,
            onComplete: function onComplete() {
                TweenLite.to(block, 0.4, { autoAlpha: 0 });
            }
        });
    }

    $cont.find(".b-scheme__close-btn:first").on('click', function (event) {
        event.preventDefault();
        hideScheme();
    });

    $cont.find(".b-contacts__readmore-link--scheme:first, #showSchemeLinkMobile").on('click', function (event) {
        event.preventDefault();
        showScheme();
    });

    $('.b-mobile-fullscreen-map__close-icon:first').on('click', function (event) {
        event.preventDefault();
        hideMobileContactsMap();
    });

    $cont.find("#showRouteToBuildingsForCarBtn").on('click', function (event) {
        event.preventDefault();
        showMobileContactsMap();
        bindMapRouteRenderOnClick(mobileMap, "DRIVING");
    });

    $cont.find("#showRouteToBuildingsForTransitBtn").on('click', function (event) {
        event.preventDefault();
        showMobileContactsMap();
        bindMapRouteRenderOnClick(mobileMap, "TRANSIT");
    });

    $cont.find('#showCarRouteOnBigMapBtn').on('click', function (event) {
        event.preventDefault();
        bindMapRouteRenderOnClick(map, "DRIVING");
    });

    $cont.find('#showTransitRouteOnBigMapBtn').on('click', function (event) {
        event.preventDefault();
        bindMapRouteRenderOnClick(map, "TRANSIT");
    });

    $cont.on('transitStart', function (event) {
        $(window).off('.contactsPage');
        hideMobileContactsMap();
        orderTransportForm.destroy();
        writeUsForm.destroy();
        writeUsForm = orderTransportForm = null;
    }).on('transitEnd', function (event) {
        $mobileMap.remove();
        $("#contactsMap").remove();
        map = mobileMap = null;
    });
})();