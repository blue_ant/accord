"use strict";

(function () {

    var $cont = $(".l-baby:first").parent();
    var bgEl1 = $cont.find(".b-parralax-bg__track--baby-1:first").get(0);
    var bgEl2 = $cont.find(".b-parralax-bg__track--baby-2:first").get(0);
    var $col = $cont.find('.b-centered-text:first');

    $col.on('scroll', function (event) {
        var $col = $(event.target);
        var onePercent = $col.height() / 100;
        var currentScroll = $col.scrollTop();
        var slip = parseInt(currentScroll / onePercent);

        TweenLite.set(bgEl1, { y: -slip * 2 });
        TweenLite.set(bgEl2, { y: -slip });
    }).niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "#d3d3d3",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }

    });

    $cont.on("transitEnd", function () {
        $col.getNiceScroll().remove();
    });
})();