"use strict";

(function () {
    $(".b-popup-gallery").appendTo(".l-diary:first");
    var allSwipers = [];
    var $lineSliders = $(".b-gallery__img-slider");
    var $popupSliders = $(".b-popupgallery__slider");

    var primeSwiper = new Swiper('.b-gallery__primeslider', {
        effect: 'fade',
        fade: {
            crossFade: true
        },
        nextButton: '.b-gallery__mobswitcher-navbtn--next',
        prevButton: '.b-gallery__mobswitcher-navbtn--prev',
        onlyExternal: true,
        onTransitionStart: function onTransitionStart(swiper) {
            var $titles = $('.b-gallery__title');
            var num = swiper.realIndex;
            $titles.filter('.b-gallery__title--active:first').removeClass('b-gallery__title--active');
            $titles.eq(num).addClass('b-gallery__title--active');
        },
        onInit: function onInit(swiper) {
            $(".b-gallery__title ").on('click', function (event) {
                event.preventDefault();
                swiper.slideTo($(this).index());
            });
        }
    });

    allSwipers.push(primeSwiper);

    $lineSliders.each(function (index, el) {
        var $slider = $(el);
        var $controls = $slider.next();
        var $popupSlider = $popupSliders.eq(index);
        var $popupSliderModal = $popupSlider.parent().parent();

        var popupSwiper = new Swiper($popupSliders.eq(index), {
            slidesPerView: 1,
            centeredSlides: true,
            touchEventsTarget: 'slide',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            nextButton: $popupSliderModal.find('.b-popup-gallery__navbtn--next'),
            prevButton: $popupSliderModal.find('.b-popup-gallery__navbtn--prev'),
            loop: false
        });

        var swiper = new Swiper($slider, {
            pagination: $controls.find('.b-gallery__pager:first'),
            slidesPerView: 'auto',
            centeredSlides: true,
            paginationClickable: true,
            paginationType: 'fraction',
            touchEventsTarget: 'wrapper',
            lazyLoading: true,
            lazyLoadingInPrevNext: true,
            lazyLoadingInPrevNextAmount: 3,
            nextButton: $controls.find('.b-gallery__navbtn--next'),
            prevButton: $controls.find('.b-gallery__navbtn--prev'),
            paginationFractionRender: function paginationFractionRender(swiper, currentClassName, totalClassName) {
                return '<div class="b-gallery__counter b-gallery__counter--current ' + currentClassName + ' "></div><div class="b-gallery__counter b-gallery__counter--total ' + totalClassName + ' "></div>';
            },
            onTransitionEnd: function onTransitionEnd(swiper) {
                popupSwiper.slideTo(swiper.realIndex, 0);
            }
        });

        $popupSliderModal.on('shown.bs.modal', function (e) {
            popupSwiper.update();
        });

        allSwipers.push(swiper, popupSwiper);
    });

    $(".b-gallery__slide").on('click', function (event) {
        if ($(window).width() < 768) {
            return false;
        }
    });

    $(".l-gallery:first").parent().on('transitStart', function (event) {
        $(".b-popup-gallery.in").removeClass('fade').modal("hide");
    }).on('transitEnd', function (event) {

        for (var i = allSwipers.length - 1; i >= 0; i--) {
            allSwipers[i].destroy(true, false);
        }

        allSwipers = null;
    });
})();