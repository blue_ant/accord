"use strict";

(function () {

    var $cont = $(".l-infras--external:first").parent();

    var accordBuildingsCoords = {
        lat: 55.607676,
        lng: 37.104740
    };
    var map = new google.maps.Map(document.getElementById('infrasMap'), {
        zoom: 12,
        center: { lat: accordBuildingsCoords.lat + 0.01, lng: accordBuildingsCoords.lng },
        streetViewControl: false
    });
    var markers = _.sortBy(window.accordAppStates.ifrasObjs, function (o) {
        return o.coords.lat;
    });

    var insertedMarkers = [];

    var _loop = function _loop(i) {

        var image = {
            url: "http://acc.blue-ant.ru" + markers[i].ico,
            scaledSize: new google.maps.Size(104, 58),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(78, 58)
        };
        var infowindow = new google.maps.InfoWindow({
            content: markers[i].title,
            pixelOffset: new google.maps.Size(26, 0)
        });
        var marker = new google.maps.Marker({
            position: markers[i].coords,
            map: map,
            icon: image,
            iconsType: markers[i].type
        });
        marker.addListener('click', function () {
            infowindow.open(map, marker);
        });

        insertedMarkers.push(marker);
    };

    for (var i = 0; i < markers.length; i++) {
        _loop(i);
    }

    var mainPin = {
        url: "/img/contacts/logo_contact.png",
        scaledSize: new google.maps.Size(134, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(100, 100)

    };
    var mainMarker = new google.maps.Marker({
        position: accordBuildingsCoords,
        map: map,
        icon: mainPin,
        iconsType: "unbeatable"
    });

    insertedMarkers.push(mainMarker);

    $cont.find(".b-taglist__link").on('click', function (event) {
        event.preventDefault();
        var $tag = $(event.currentTarget);
        var $pins = $(".b-contacts-map__pin");

        $tag.toggleClass('b-taglist__link--active');

        var $activeTags = $(".b-taglist__link--active");
        if (!$activeTags.length) {
            for (var i = insertedMarkers.length - 1; i >= 0; i--) {
                insertedMarkers[i].setVisible(true);
            }
        } else {
            var activeTypes = [];
            $activeTags.each(function (index, el) {
                activeTypes.push(parseInt(el.dataset.iconsType));
            });

            for (var _i = insertedMarkers.length - 1; _i >= 0; _i--) {
                if (insertedMarkers[_i].iconsType === "unbeatable") {
                    continue;
                } else if (_.includes(activeTypes, insertedMarkers[_i].iconsType)) {
                    insertedMarkers[_i].setVisible(true);
                } else {
                    insertedMarkers[_i].setVisible(false);
                }
            }
        }
    });

    $cont
    /* .on('transitStart', (event) => {
      })*/
    .on('transitEnd', function (event) {
        map = insertedMarkers = null;
    });
})();