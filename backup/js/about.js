'use strict';

(function () {
    var $col = $('.b-aboutpage__text:first');
    $('.b-aboutpage__text').niceScroll({
        cursorcolor: "#d3d3d3",
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true
    });

    var $cont = $(".l-about:first").parent();
    $cont.on("transitEnd", function () {
        $col.getNiceScroll().remove();
    });
})();