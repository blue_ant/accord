"use strict";

(function () {
    var $cont = $(".l-article:first");
    var $popupSliderModal = $cont.find("#articleGallery");

    var articleMobileSlider = new Swiper('#articleMobileSlider', {
        slidesPerView: 1,
        centeredSlides: true,
        touchEventsTarget: 'slide',
        lazyLoading: true,
        lazyLoadingInPrevNext: true,
        lazyLoadingInPrevNextAmount: 2,
        loop: false
    });

    var gallerySlider = new Swiper('#articleGallerySlider', {
        slidesPerView: 1,
        centeredSlides: true,
        touchEventsTarget: 'slide',
        lazyLoading: true,
        lazyLoadingInPrevNext: true,
        lazyLoadingInPrevNextAmount: 2,
        nextButton: $popupSliderModal.find('.b-popup-gallery__navbtn--next'),
        prevButton: $popupSliderModal.find('.b-popup-gallery__navbtn--prev'),
        loop: false
    });

    $popupSliderModal.on('shown.bs.modal', function (e) {
        gallerySlider.update();
    });

    $cont.find("a[data-slide-num]").on('click', function (event) {
        gallerySlider.slideTo(parseInt(this.dataset.slideNum), 0);
    });

    $('.l-action .b-centered-text:first').niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "rgb(229,237,242)",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }
    });

    var $col = $cont.find('.b-centered-text:first');
    $col.niceScroll({
        cursorcolor: "#d3d3d3",
        cursorborder: "#d3d3d3",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        railoffset: { top: -8 }
    });
    $cont.on('transitStart', function (event) {
        $(".b-popup-gallery.in").removeClass('fade').modal("hide");
    }).on('transitEnd', function (event) {
        $col.getNiceScroll().remove();
        gallerySlider.destroy(true, true);
        articleMobileSlider.destroy(true, true);
        gallerySlider = articleMobileSlider = null;
    });
})();