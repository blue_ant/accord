"use strict";

(function () {
    insertGenplanModal();

    var $genplanModal = $("#genplanModal");

    var $cont = $(".l-storerooms:first").parent();
    var getFlatForm = new InteractiveForm({ el: "#form6", successTextClass: "b-aside-form__success-text" });
    var scroller1 = $cont.find('.b-flats:first').niceScroll({
        cursorcolor: "#ffffff",
        cursorborder: "1px solid #bbbcbc",
        cursorwidth: "5px",
        autohidemode: false,
        enablekeyboard: false,
        smoothscroll: true,
        horizrailenabled: false,
        background: "#dfdbda"
    });

    var scroller2 = $cont.find("#storeroomsPageFiltersBlock").niceScroll({
        autohidemode: false,
        enablekeyboard: false,
        horizrailenabled: false,
        cursoropacitymin: 0,
        cursoropacitymax: 0,
        smoothscroll: false
    });

    var rangeSlider1 = $cont.find('[data-area-slider]:first').slider({
        tooltip: 'hide'
    }).on("slide", function (event) {
        var min = event.value[0];
        var max = event.value[1];

        $("#storeroomsAreaMin").html(min);
        $("#storeroomsAreaMax").html(max);
        $cont.find('[name="area_from"]').val(min);
        $cont.find('[name="area_to"]').val(max);
    });

    var storeroomCellTpl = Handlebars.compile("\n\n{{#if flats}}\n{{#each flats}}\n<div class=\"b-flats__cell col-xs-12 col-sm-6 col-md-4\">\n    <div class=\"b-flats__cell-inner\">\n        <div class=\"b-flatcard hidden-xs\">\n            <div class=\"b-flatcard__row\">\n                <div class=\"b-flatcard__cell\">\n                    <div class=\"va\">\n                        <div class=\"va-item b-flatcard__title\">\u2116{{name}}</div>\n                        <div class=\"va-item b-flatcard__house-title\">\u043A\u043E\u0440\u043F\u0443\u0441&nbsp;{{house}}</div>\n                    </div>\n                </div>\n            </div>\n            <div class=\"b-flatcard__row b-flatcard__row--middle\">\n                <div class=\"b-flatcard__cell\">\n                    <div class=\"b-flatcard__img b-flatcard__img--with-btn\" style=\"background-image: url({{img}});\"> <a href=\"#\" data-aside-menu=\"getPriceAsideForm\" class=\"btn btn-xl btn-primary b-flatcard__btn\">\u0423\u0437\u043D\u0430\u0442\u044C \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u044C</a> </div>\n                </div>\n            </div>\n            <div class=\"b-flatcard__row\">\n                <div class=\"b-flatcard__cell\">\n                    <div class=\"va\">\n                        <div class=\"va-item b-flatcard__footage\"> {{area}} m<sup>2</sup></div>\n                        <div class=\"va-item b-flatcard__entrance-num\">\u043F\u043E\u0434\u044A\u0435\u0437\u0434&nbsp;{{section}}</div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"b-mobile-flatcard container-fluid visible-xs\">\n            <div class=\"row\">\n                <div class=\"col-xs-7\"> <img src=\"{{img}}\" alt=\"\" class=\"b-mobile-flatcard__img img-responsive center-block\"> </div>\n                <div class=\"col-xs-5\">\n                    <div class=\"b-mobile-flatcard__title\">\u2116{{name}}</div>\n                    <div class=\"b-mobile-flatcard__footage\"> {{area}} m<sup>2</sup></div>\n                    <div class=\"b-mobile-flatcard__footage\">\u043A\u043E\u0440\u043F\u0443\u0441&nbsp;{{house}}</div>\n                    <div class=\"b-mobile-flatcard__footage\">\u043F\u043E\u0434\u044A\u0435\u0437\u0434&nbsp;{{section}}</div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n{{/each}}\n\n{{else}}\n<div class=\"b-flats__empty\">\n    <div class=\"b-flats__empty-inner\">\n        <img src=\"/img/cloud.svg\" alt=\"\" class=\"img-responsive\">\n        <div class=\"b-flats__empty-header\">\u041D\u0415\u0422 \u0420\u0415\u0417\u0423\u041B\u042C\u0422\u0410\u0422\u041E\u0412</div>\n        <div class=\"b-flats__empty-text\">\u041F\u043B\u0430\u043D\u0438\u0440\u043E\u0432\u043A\u0438 \u043F\u043E \u0437\u0430\u0434\u0430\u043D\u043D\u044B\u043C \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u0430\u043C \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u044B,<br>\n\u043F\u043E\u0436\u0430\u043B\u0443\u0439\u0441\u0442\u0430 \u0438\u0437\u043C\u0435\u043D\u0438\u0442\u0435 \u043F\u0430\u0440\u0430\u043C\u0435\u0442\u0440\u044B \u043F\u043E\u0438\u0441\u043A\u0430.</div>\n        <img src=\"/img/crane.svg\" alt=\"\" class=\"b-flats__empty-bootom-img\">\n    </div>\n</div>\n{{/if}}\n\n");

    $(document).on('click.storeroomspage', '.b-flats-filter__apply-btn', function (event) {
        event.preventDefault();
        $cont.find(".b-flats-filter__close:first").trigger('click');
        return false;
    });

    var $filterForm = $cont.find("#formFilterStorerooms");
    restoreFiltersParams({
        form: $filterForm,
        storageID: "flatsFiltersFormStorage",
        skip: ["area_to", "area_from"]
    });

    $filterForm.find("input").on("change", _.debounce(function () {
        var $nonEmptyInputs = $filterForm.find("input").filter(function (index, element) {
            return $(element).val() !== "";
        });
        preserveFiltersParams({
            inputs: $nonEmptyInputs,
            storageID: "flatsFiltersFormStorage",
            skip: ["area_to", "area_from"]
        });
        $filterForm.trigger('submit');
    }, 100));

    $filterForm.on('submit', function (event) {
        event.preventDefault();
        showPagePreloader();
        var $nonEmptyInputs = $filterForm.find("input").filter(function (index, element) {
            return $(element).val() !== "";
        });

        var urlParams = $nonEmptyInputs.serialize();
        if (urlParams.length) {
            urlParams = "&" + urlParams;
        }

        $.ajax({
            url: '/storerooms/?ajax=1&get_flats=1&filter=1' + urlParams

        }).done(function (data) {
            var markup = storeroomCellTpl(data);
            $cont.find("#storeroomCellsContainer").html(markup);
            window.accordAppStates.isPreloaderLockedOn = false;
            hidePagePreloader();
        });

        return false;
    });

    $filterForm.trigger('submit');

    $cont.on('transitStart', function (event) {
        genplanHide();
        window.accordAppStates.isPreloaderLockedOn = false;
        $(".modal.in").removeClass('fade').modal("hide").addClass('fade');
        $([window, document]).off(".storeroomspage");
    }).on('transitEnd', function (event) {

        rangeSlider1.slider('destroy');

        scroller1.remove();
        scroller2.remove();
        getFlatForm.destroy();
        getFlatForm = null;
    });
})();